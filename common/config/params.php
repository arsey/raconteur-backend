<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'repositoriesFolders' => [
        'avatars' => '@mainRoot/uploads/avatars'
    ]
];
