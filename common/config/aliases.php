<?php
Yii::setAlias('common', dirname(__DIR__));
Yii::setAlias('mainRoot', dirname(dirname(__DIR__)));
Yii::setAlias('mainRootWeb', dirname(dirname(__DIR__)).'/web');
Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('api', dirname(dirname(__DIR__)) . '/api');
