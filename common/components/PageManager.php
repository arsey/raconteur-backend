<?php

namespace common\components;

class PageManager
{

    private $scriptsDir = 'js';
    private $stylesDir = 'css';
    private $imagesDir = 'images';
    private $tempDir = null;
    private $serverName = 'http://backend-raconteur.loc/';

    public static $iso = array(
        "Є" => "YE", "І" => "I", "Ѓ" => "G", "і" => "i", "№" => "#", "є" => "ye", "ѓ" => "g",
        "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
        "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
        "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
        "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
        "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "X",
        "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
        "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
        "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
        "е" => "e", "ё" => "yo", "ж" => "zh",
        "з" => "z", "и" => "i", "й" => "j", "к" => "k", "л" => "l",
        "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
        "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "x",
        "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
        "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya",
        "—" => "-", "«" => "", "»" => "", "…" => ""
    );

    public function actionHtml($id)
    {
        $model = $this->findModel($id);
        echo $model->html;
    }

    private function getTempDir()
    {
        $t = sys_get_temp_dir();
        $newDirPath = $t . DIRECTORY_SEPARATOR . uniqid();
        mkdir($newDirPath, 0777);
        return $newDirPath;
    }

    private function saveScripts()
    {
        $scriptsDir = $this->tempDir . DIRECTORY_SEPARATOR . $this->scriptsDir;
        mkdir($scriptsDir, 0777);

        $selector = 'script[src*="raconteur"]';
        foreach (pq($selector) as $key => $s) {
            $src = $s->getAttribute('src');
            $fileNameParts = explode('/', $src);
            $fileName = $fileNameParts[count($fileNameParts) - 1];
            $fileContent = @file_get_contents($src);
            if ($fileContent) {
                file_put_contents($scriptsDir . DIRECTORY_SEPARATOR . $fileName, $fileContent);
            }

            $s->setAttribute('src', $this->scriptsDir . DIRECTORY_SEPARATOR . $fileName);
        }
    }

    private function saveCss()
    {
        $dir = $this->tempDir . DIRECTORY_SEPARATOR . $this->stylesDir;
        mkdir($dir, 0777);
        foreach (pq('link[type="text/css"][href*="raconteur"]') as $s) {
            $path = $s->getAttribute('href');
            $fileNameParts = explode('/', $path);
            $fileName = $fileNameParts[count($fileNameParts) - 1];
            $fileContent = @file_get_contents($path);
            if ($fileContent) {
                file_put_contents($dir . DIRECTORY_SEPARATOR . $fileName, $fileContent);
            }

            $s->setAttribute('href', $this->stylesDir . DIRECTORY_SEPARATOR . $fileName);
        }
    }

    private function saveImages()
    {
        $dir = $this->tempDir . DIRECTORY_SEPARATOR . $this->imagesDir;
        mkdir($dir, 0777);
        foreach (pq('img[src*="raconteur"]') as $key => $s) {
            $src = $s->getAttribute('src');
            $fileNameParts = explode('/', $src);
            $fileName = $fileNameParts[count($fileNameParts) - 1];
            $fileContent = @file_get_contents($src);
            if ($fileContent) {
                file_put_contents($dir . DIRECTORY_SEPARATOR . $fileName, $fileContent);
            }

            $s->setAttribute('src', $this->imagesDir . DIRECTORY_SEPARATOR . $fileName);
        }
    }

    private function saveIndexFile()
    {
        $dir = $this->tempDir;
        file_put_contents($dir . DIRECTORY_SEPARATOR . 'index.html', pq('html'));
    }

    public function make($html)
    {
        \phpQuery::newDocument($html);

        $this->tempDir = $this->getTempDir();

        $this->saveScripts();
        $this->saveCss();
        $this->saveImages();

        $this->saveIndexFile();

        return $this;
    }


    private function recurse_copy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    $this->recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }


    public function publish($dst)
    {
        $this->recurse_copy($this->tempDir, $dst);
        return $this;
    }

    function zip($destination)
    {
        $source = $this->tempDir;

        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new \ZipArchive();
        if (!$zip->open($destination, \ZipArchive::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));

        if (is_dir($source) === true) {
            $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($source), \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if (in_array(substr($file, strrpos($file, '/') + 1), array('.', '..')))
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($source) === true) {
            $zip->addFromString(basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

    public static function rrmdir($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") self::rrmdir($dir . "/" . $object); else unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function deleteTempDir()
    {
        self::rrmdir($this->tempDir);
    }

    public static function sanitizeWithTranslit($text)
    {
        return strtr($text, self::$iso);
    }

    // Give me a slug form title
    public static function sluggify($url)
    {
        # Prep string with some basic normalization
        $url = strtolower($url);
        $url = strip_tags($url);
        $url = stripslashes($url);
        $url = html_entity_decode($url);
        # Remove quotes (can't, etc.)
        $url = str_replace('\'', '', $url);
        # Replace non-alpha numeric with hyphens
        $match = '/[^a-z0-9]+/';
        $replace = '-';
        $url = preg_replace($match, $replace, $url);
        $url = trim($url, '-');
        return $url;
    }

}