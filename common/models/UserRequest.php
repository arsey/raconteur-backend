<?php

namespace common\models;

use yii\behaviors\TimestampBehavior;

class UserRequest extends \common\models\base\BaseUserRequest
{

    const TYPE_NEW_TEMPLATE = 'new_template';
    const TYPE_UPGRADE_PLAN = 'upgrade_plan';

    public static $types = [
        self::TYPE_NEW_TEMPLATE => 'new template',
        self::TYPE_UPGRADE_PLAN => 'upgrade plan'
    ];

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';
    const STATUS_DECLINED = 'declined';

    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',

            ]
        ];
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['type'], 'validateType'];
        return $rules;
    }

    public function validateType($attribute)
    {
        if (!in_array($this->{$attribute}, [self::TYPE_NEW_TEMPLATE, self::TYPE_UPGRADE_PLAN])) {
            $this->addError($attribute, 'The type must be either "new_template", or "upgrade_plan"');
        }
    }

    public function fields()
    {
        return [
            'id',
            'text',
            'type',
            'created',
            'modified',
            'status'
        ];
    }
}