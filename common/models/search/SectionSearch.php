<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Section;

/**
 * SectionSearch represents the model behind the search form about `common\models\Section`.
 */
class SectionSearch extends Section
{

    public $template;
    public $template_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created', 'modified'], 'integer'],
            [['title','tags', 'preview_img', 'html', 'css', 'shared', 'type', 'status', 'template', 'template_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Section::find();

        $query->joinWith(['template']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['template'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['template.title' => SORT_ASC],
            'desc' => ['template.title' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'section.id' => $this->id,
            'created' => $this->created,
            'modified' => $this->modified,
        ]);

        $query->andFilterWhere(['like', 'section.title', $this->title])
            ->andFilterWhere(['like', 'preview_img', $this->preview_img])
            ->andFilterWhere(['like', 'html', $this->html])
            ->andFilterWhere(['like', 'css', $this->css])
            ->andFilterWhere(['like', 'shared', $this->shared])
            ->andFilterWhere(['like', 'section.type', $this->type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'template.title', $this->template])
            ->andFilterWhere(['like', 'tags', $this->tags])
            ->andFilterWhere(['template.id'=> $this->template_id]);

        return $dataProvider;
    }
}
