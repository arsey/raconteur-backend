<?php

namespace common\models;

use backend\models\Mediafile;
use \common\models\base\BaseScript;

class Script extends BaseScript
{
    public $files = [];

    public static $types = [
        'js' => 'javascript',
        'css' => 'css'
    ];

    public function fields()
    {
        return [
            'name',
            'description',
            'type',
            'file'
        ];
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), ['file_id' => 'Script File']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Mediafile::className(), ['id' => 'file_id']);
    }

}