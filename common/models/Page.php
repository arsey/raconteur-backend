<?php

namespace common\models;

use common\models\base\BasePage;
use yii\behaviors\TimestampBehavior;
use common\models\Template;
use backend\models\User;
use yii;


class Page extends BasePage
{
    const STATUS_PUBLISHED = 'published';
    const STATUS_UNPUBLISHED = 'unpublished';
    const STATUS_REMOVED = 'removed';

    public static $statusLabels = [
        self::STATUS_PUBLISHED => 'Published',
        self::STATUS_UNPUBLISHED => 'Unpublished',
        self::STATUS_REMOVED => 'Removed',
    ];

    public $url = null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['template_id'], 'checkTemplateId'];
        return $rules;
    }

    public function fields()
    {
        return [
            'id',
            'template_id',
            'title',
            'header',
            'content',
            'navigation',
            'html',
            'url_name',
            'status',
            'created',
            'modified'
        ];
    }

    public function checkTemplateId($attribute, $params)
    {
        $allowedTemplates = Template::getUserAllowedTemplatesIds(Yii::$app->user->id);
        if (!in_array($this->{$attribute}, $allowedTemplates)) {
            $this->addError('template_id', 'Wrong template ID');
        } else if (!Template::find()->where(['id' => $this->{$attribute}])->one()) {
            $this->addError('template_id', 'Template not found');
        }
    }

    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',

            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }

    public static function getUserNumberSavedPages($userId)
    {
        return self::find()->where(['user_id' => $userId, 'status' => [Page::STATUS_PUBLISHED, Page::STATUS_UNPUBLISHED]])->count();
    }

    public static function getUserNumberPublishedPages($userId)
    {
        $q = self::find();
        $q->where(['user_id' => $userId, 'status' => Page::STATUS_PUBLISHED]);
        return $q->count();
    }
}