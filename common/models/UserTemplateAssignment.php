<?php

namespace common\models;

use common\models\base\BaseUserTemplateAssignment;

class UserTemplateAssignment extends BaseUserTemplateAssignment
{
    public static function getUserAssignedTemplatesIds($userId)
    {
        return self::find()->select('template_id')->where(['user_id' => $userId])->column();
    }

    public static function assignToUser($userId, array $templateIds)
    {
        /** remove previous assignments */
        $assignments = self::find()->where("user_id={$userId}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save new assignments
         */
        if (!empty($templateIds)) {
            foreach ($templateIds as $id) {
                $r = new self();
                $r->user_id = $userId;
                $r->template_id = $id;
                $r->save();
            }
        }
    }
}