<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "script".
 *
 * @property integer $id
 * @property integer $file_id
 * @property string $name
 * @property string $description
 * @property string $type
 *
 * @property ScriptSectionAssignment[] $scriptSectionAssignments
 * @property Section[] $sections
 * @property ScriptTemplateAssignment[] $scriptTemplateAssignments
 * @property Template[] $templates
 */
class BaseScript extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['file_id', 'name', 'type'], 'required'],
            [['file_id'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_id' => 'File ID',
            'name' => 'Name',
            'description' => 'Description',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptSectionAssignments()
    {
        return $this->hasMany(ScriptSectionAssignment::className(), ['script_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['id' => 'section_id'])->viaTable('script_section_assignment', ['script_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptTemplateAssignments()
    {
        return $this->hasMany(ScriptTemplateAssignment::className(), ['script_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(Template::className(), ['id' => 'template_id'])->viaTable('script_template_assignment', ['script_id' => 'id']);
    }
}
