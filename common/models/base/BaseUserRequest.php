<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "user_request".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property string $type
 * @property integer $created
 * @property integer $modified
 * @property string $status
 *
 * @property User $user
 */
class BaseUserRequest extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type'], 'required'],
            [['user_id', 'created', 'modified'], 'integer'],
            [['type', 'status'], 'string'],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'text' => 'Text',
            'type' => 'Type',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
