<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "template_section_assignment".
 *
 * @property integer $template_id
 * @property integer $section_id
 *
 * @property Section $section
 * @property Template $template
 */
class BaseTemplateSectionAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template_section_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'section_id'], 'required'],
            [['template_id', 'section_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'section_id' => 'Section ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }
}
