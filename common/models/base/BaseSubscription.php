<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $period
 * @property integer $pages_allowed
 * @property integer $downloads_allowed
 * @property integer $publishes_allowed
 * @property string $price
 * @property integer $created
 * @property integer $modified
 * @property string $status
 *
 * @property SubscriptionTemplateAssignment[] $subscriptionTemplateAssignments
 * @property Template[] $templates
 * @property SubscriptionUserAssignment[] $subscriptionUserAssignments
 * @property User[] $users
 */
class BaseSubscription extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'period'], 'required'],
            [['description', 'status'], 'string'],
            [['pages_allowed', 'downloads_allowed', 'publishes_allowed', 'created', 'modified'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['period'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Plan Name',
            'description' => 'Description',
            'period' => 'Period of activity',
            'pages_allowed' => 'Saved pages allowed',
            'downloads_allowed' => 'Downloads Allowed',
            'publishes_allowed' => 'Publishes Allowed',
            'price' => 'Price',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionTemplateAssignments()
    {
        return $this->hasMany(SubscriptionTemplateAssignment::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(Template::className(), ['id' => 'template_id'])->viaTable('subscription_template_assignment', ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionUserAssignments()
    {
        return $this->hasMany(SubscriptionUserAssignment::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('subscription_user_assignment', ['subscription_id' => 'id']);
    }
}
