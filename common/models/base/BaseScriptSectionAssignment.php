<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "script_section_assignment".
 *
 * @property integer $script_id
 * @property integer $section_id
 * @property integer $order
 *
 * @property Script $script
 * @property Section $section
 */
class BaseScriptSectionAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_section_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['script_id', 'section_id'], 'required'],
            [['script_id', 'section_id', 'order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'script_id' => 'Script ID',
            'section_id' => 'Section ID',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScript()
    {
        return $this->hasOne(Script::className(), ['id' => 'script_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }
}
