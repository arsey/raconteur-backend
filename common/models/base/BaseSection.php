<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property integer $id
 * @property string $title
 * @property string $preview_img
 * @property string $html
 * @property string $css
 * @property string $tags
 * @property string $shared
 * @property string $type
 * @property integer $created
 * @property integer $modified
 * @property string $status
 *
 * @property ScriptSectionAssignment[] $scriptSectionAssignments
 * @property Script[] $scripts
 * @property SectionImageAssignment[] $sectionImageAssignments
 * @property FilemanagerMediafile[] $files
 * @property TemplateSectionAssignment[] $templateSectionAssignments
 * @property Template[] $templates
 */
class BaseSection extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'type'], 'required'],
            [['html', 'css', 'shared', 'status'], 'string'],
            [['created', 'modified'], 'integer'],
            [['title', 'preview_img', 'tags'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'preview_img' => 'Preview Img',
            'html' => 'Html',
            'css' => 'Css',
            'tags' => 'Tags',
            'shared' => 'Shared',
            'type' => 'Type',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptSectionAssignments()
    {
        return $this->hasMany(ScriptSectionAssignment::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScripts()
    {
        return $this->hasMany(Script::className(), ['id' => 'script_id'])->viaTable('script_section_assignment', ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionImageAssignments()
    {
        return $this->hasMany(SectionImageAssignment::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(FilemanagerMediafile::className(), ['id' => 'file_id'])->viaTable('section_image_assignment', ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateSectionAssignments()
    {
        return $this->hasMany(TemplateSectionAssignment::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplates()
    {
        return $this->hasMany(Template::className(), ['id' => 'template_id'])->viaTable('template_section_assignment', ['section_id' => 'id']);
    }
}
