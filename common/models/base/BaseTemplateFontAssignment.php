<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "template_font_assignment".
 *
 * @property integer $template_id
 * @property integer $file_id
 *
 * @property Template $template
 * @property FilemanagerMediafile $file
 */
class BaseTemplateFontAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template_font_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id', 'file_id'], 'required'],
            [['template_id', 'file_id'], 'integer'],
            [['template_id', 'file_id'], 'unique', 'targetAttribute' => ['template_id', 'file_id'], 'message' => 'The combination of Template ID and File ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'template_id' => 'Template ID',
            'file_id' => 'File ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(FilemanagerMediafile::className(), ['id' => 'file_id']);
    }
}
