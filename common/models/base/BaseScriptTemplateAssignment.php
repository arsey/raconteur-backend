<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "script_template_assignment".
 *
 * @property integer $script_id
 * @property integer $template_id
 * @property integer $order
 *
 * @property Script $script
 * @property Template $template
 */
class BaseScriptTemplateAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'script_template_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['script_id', 'template_id'], 'required'],
            [['script_id', 'template_id', 'order'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'script_id' => 'Script ID',
            'template_id' => 'Template ID',
            'order' => 'Order',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScript()
    {
        return $this->hasOne(Script::className(), ['id' => 'script_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }
}
