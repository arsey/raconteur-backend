<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "subscription_user_assignment".
 *
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $user_id
 * @property integer $date_from
 * @property integer $end_date
 * @property integer $pages_saved
 * @property integer $pages_published
 * @property integer $downloads
 * @property string $status
 *
 * @property Subscription $subscription
 * @property User $user
 */
class BaseSubscriptionUserAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_user_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'user_id', 'date_from', 'end_date'], 'required'],
            [['subscription_id', 'user_id', 'date_from', 'end_date', 'pages_saved', 'pages_published', 'downloads'], 'integer'],
            [['status'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subscription_id' => 'Subscription ID',
            'user_id' => 'User ID',
            'date_from' => 'Date From',
            'end_date' => 'End Date',
            'pages_saved' => 'Pages Saved',
            'pages_published' => 'Pages Published',
            'downloads' => 'Downloads',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
