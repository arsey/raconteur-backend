<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $template_id
 * @property string $title
 * @property string $header
 * @property string $content
 * @property string $navigation
 * @property string $html
 * @property string $url_name
 * @property integer $created
 * @property integer $modified
 * @property string $status
 *
 * @property User $user
 * @property Template $template
 */
class BasePage extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'template_id', 'title', 'html'], 'required'],
            [['user_id', 'template_id', 'created', 'modified'], 'integer'],
            [['header', 'content', 'navigation', 'html', 'status'], 'string'],
            [['title', 'url_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'template_id' => 'Template ID',
            'title' => 'Title',
            'header' => 'Header',
            'content' => 'Content',
            'navigation' => 'Navigation',
            'html' => 'Html',
            'url_name' => 'sluggified from title by default also will published under URL like http://raconteur.com/ClientName/PageName ',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }
}
