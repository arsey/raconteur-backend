<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property string $title
 * @property string $comment
 * @property integer $preview_img
 * @property string $html
 * @property string $css
 * @property string $type
 * @property integer $created
 * @property integer $modified
 * @property string $status
 *
 * @property Page[] $pages
 * @property ScriptTemplateAssignment[] $scriptTemplateAssignments
 * @property Script[] $scripts
 * @property SubscriptionTemplateAssignment[] $subscriptionTemplateAssignments
 * @property Subscription[] $subscriptions
 * @property TemplateImageAssignment[] $templateImageAssignments
 * @property FilemanagerMediafile[] $files
 * @property TemplateSectionAssignment[] $templateSectionAssignments
 * @property Section[] $sections
 * @property UserTemplateAssignment[] $userTemplateAssignments
 * @property User[] $users
 */
class BaseTemplate extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'created', 'modified'], 'required'],
            [['preview_img', 'created', 'modified'], 'integer'],
            [['html', 'css', 'type', 'status'], 'string'],
            [['title'], 'string', 'max' => 150],
            [['comment'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'comment' => 'Comment',
            'preview_img' => 'Preview Img',
            'html' => 'Html',
            'css' => 'Css',
            'type' => 'Type',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPages()
    {
        return $this->hasMany(Page::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptTemplateAssignments()
    {
        return $this->hasMany(ScriptTemplateAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScripts()
    {
        return $this->hasMany(Script::className(), ['id' => 'script_id'])->viaTable('script_template_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionTemplateAssignments()
    {
        return $this->hasMany(SubscriptionTemplateAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['id' => 'subscription_id'])->viaTable('subscription_template_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateImageAssignments()
    {
        return $this->hasMany(TemplateImageAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(FilemanagerMediafile::className(), ['id' => 'file_id'])->viaTable('template_image_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateSectionAssignments()
    {
        return $this->hasMany(TemplateSectionAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(Section::className(), ['id' => 'section_id'])->viaTable('template_section_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTemplateAssignments()
    {
        return $this->hasMany(UserTemplateAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_template_assignment', ['template_id' => 'id']);
    }
}
