<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "section_image_assignment".
 *
 * @property integer $section_id
 * @property integer $file_id
 *
 * @property Section $section
 * @property FilemanagerMediafile $file
 */
class BaseSectionImageAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'section_image_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'file_id'], 'required'],
            [['section_id', 'file_id'], 'integer'],
            [['section_id', 'file_id'], 'unique', 'targetAttribute' => ['section_id', 'file_id'], 'message' => 'The combination of Section ID and File ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'section_id' => 'Section ID',
            'file_id' => 'File ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(FilemanagerMediafile::className(), ['id' => 'file_id']);
    }
}
