<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $company
 * @property string $position
 * @property string $phone
 * @property string $photo
 * @property integer $sessions
 * @property string $create_time
 * @property string $update_time
 *
 * @property User $user
 */
class BaseProfile extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name', 'company'], 'required'],
            [['user_id', 'sessions'], 'integer'],
            [['create_time', 'update_time'], 'safe'],
            [['first_name', 'last_name'], 'string', 'max' => 30],
            [['company'], 'string', 'max' => 150],
            [['position'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 45],
            [['photo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'company' => 'Company',
            'position' => 'Position',
            'phone' => 'Phone',
            'photo' => 'Photo',
            'sessions' => 'Sessions',
            'create_time' => 'Create Time',
            'update_time' => 'Update Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
