<?php

namespace common\models\base;

use Yii;

/**
 * This is the model class for table "subscription_template_assignment".
 *
 * @property integer $subscription_id
 * @property integer $template_id
 *
 * @property Subscription $subscription
 * @property Template $template
 */
class BaseSubscriptionTemplateAssignment extends \common\models\ExtActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_template_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'template_id'], 'required'],
            [['subscription_id', 'template_id'], 'integer'],
            [['subscription_id', 'template_id'], 'unique', 'targetAttribute' => ['subscription_id', 'template_id'], 'message' => 'The combination of Subscription ID and Template ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subscription_id' => 'Subscription ID',
            'template_id' => 'Template ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }
}
