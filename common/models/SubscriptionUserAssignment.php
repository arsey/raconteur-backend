<?php

namespace common\models;

use common\models\base\BaseSubscriptionUserAssignment;

class SubscriptionUserAssignment extends BaseSubscriptionUserAssignment
{
    const STATUS_ACTIVE = 'active';
    const STATUS_PAUSED = 'paused';
    const STATUS_ENDED = 'ended';
    const STATUS_INACTIVE = 'inactive';

    public static function getUserActiveSubscription($userId)
    {
        return self::find()->where(['user_id' => $userId, 'status' => self::STATUS_ACTIVE])->andWhere(['>', 'end_date', strtotime('now')])->one();
    }

    public static function assignToUser($userId, $subscription)
    {
        $activeSubscription = self::getUserActiveSubscription($userId);

        /** exit if current user subscription is the same */
        if ($subscription && $activeSubscription && $activeSubscription->subscription_id === $subscription->id)
            return;

        /* deactivate active user subscriptions */
        $assignments = self::find()->where(["user_id" => $userId, 'status' => self::STATUS_ACTIVE])->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->status = self::STATUS_INACTIVE;
                $assignment->save();
            }
        }


        /** save subscription dependencies */
        if ($subscription) {
            $r = new self();
            $r->user_id = $userId;
            $r->subscription_id = $subscription->id;
            $r->date_from = strtotime('now');
            $r->end_date = $subscription->endDateForCurrentSubscriptionFromNow;
            $r->status = self::STATUS_ACTIVE;
            $r->save();
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['id' => 'subscription_id']);
    }
}