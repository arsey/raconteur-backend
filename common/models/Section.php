<?php

namespace common\models;

use common\models\base\BaseSection;
use yii\behaviors\TimestampBehavior;
use \common\models\ScriptSectionAssignment;
use \common\models\SectionImageAssignment;
use \common\models\TemplateSectionAssignment;
use \backend\models\Mediafile;

class Section extends BaseSection
{
    public $templateIds = [];
    public $imageIds = [];
    public $scriptIds = [];
    public $previewImg = null;

    public $templates = [];
    public $scripts = [];

    /** @var array $dependencies uses to extend api response on view request */
    public $dependencies = ['scripts' => [], 'images' => []];

    public static $types = [
        'footer' => 'Footer',
        'navigation' => 'Navigation',
        'content' => 'Content',
//        'header' => 'Header'
    ];

    public function fields()
    {
        return [
            'id',
            'title',
            'preview_img',
            'html',
            'css',
            'tags',
            'type',
            'dependencies',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules=parent::rules();
        $rules[]=[['scriptIds', 'imageIds', 'templateIds'], 'safe'];
        return $rules;
    }

    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
            ]
        ];
    }


    public function afterSave($insert, $changedAttributes)
    {
        /**
         * remove templates dependencies
         */
        $assignments = TemplateSectionAssignment::find()->where("section_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save templates dependencies
         */
        if (!empty($this->templateIds)) {
            if (is_array($this->templateIds)) {
                foreach ($this->templateIds as $id) {
                    $r = new TemplateSectionAssignment();
                    $r->section_id = $this->id;
                    $r->template_id = $id;
                    $r->save();
                }
            } else {
                $r = new TemplateSectionAssignment();
                $r->section_id = $this->id;
                $r->template_id = $this->templateIds;
                $r->save();
            }
        }

        /**
         * remove scripts dependencies
         */
        $assignments = ScriptSectionAssignment::find()->where("section_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save scripts dependencies
         */
        if (!empty($this->scriptIds)) {
            $o = 0;
            $this->scriptIds = array_unique($this->scriptIds);
            foreach ($this->scriptIds as $id) {
                $r = new ScriptSectionAssignment();
                $r->section_id = $this->id;
                $r->script_id = $id;
                $r->order = $o;
                $r->save();
                $o++;
            }
        }

        /**
         * remove images dependencies
         */
        $assignments = SectionImageAssignment::find()->where("section_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save images dependencies
         */
        if (!empty($this->imageIds)) {
            $this->imageIds = array_unique($this->imageIds);
            foreach ($this->imageIds as $id) {
                $tia = new SectionImageAssignment();
                $tia->section_id = $this->id;
                $tia->file_id = $id;
                $tia->save();
            }
        }


        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptSectionAssignments()
    {
        return $this->hasMany(ScriptSectionAssignment::className(), ['section_id' => 'id'])->orderBy('order');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionImageAssignments()
    {
        return $this->hasMany(SectionImageAssignment::className(), ['section_id' => 'id']);
    }

    public function getFilesAssigned()
    {
        return $this->hasMany(Mediafile::className(), ['id' => 'file_id'])->viaTable('section_image_assignment', ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptsAssigned()
    {
        return $this->hasMany(Script::className(), ['id' => 'script_id'])->viaTable('script_section_assignment', ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id'])->viaTable('template_section_assignment', ['section_id' => 'id']);
    }

    public function getPreviewImageModel()
    {
        if ($this->preview_img) {
            $previewImg = Mediafile::find()->where('id=:id', [':id' => $this->preview_img])->one();
            $this->previewImg = $previewImg;
        }

        return $this->previewImg;
    }

    public function fetchDependencies()
    {
        $this->dependencies['images'] = $this->filesAssigned;
        $this->dependencies['scripts'] = $this->scriptsAssigned;

        return $this->dependencies;
    }

}