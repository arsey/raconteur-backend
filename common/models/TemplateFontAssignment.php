<?php

namespace common\models;

use backend\models\Mediafile;
use common\models\base\BaseTemplateFontAssignment;

class TemplateFontAssignment extends BaseTemplateFontAssignment
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Mediafile::className(), ['id' => 'file_id']);
    }
}