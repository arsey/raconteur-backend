<?php

namespace common\models;

use common\models\base\BaseSubscription;
use yii\behaviors\TimestampBehavior;
use common\models\SubscriptionTemplateAssignment;
use common\models\Template;
use backend\models\User;


class Subscription extends BaseSubscription
{
    public $templateIds = [];

    public $templates = [];

    public static $periods = [
        '1_month' => '1 month',
        '2_month' => '2 months',
        '3_month' => '3 months',
        '6_month' => '6 months',
        '12_month' => '12 months',
        '0' => 'unlimited',
    ];


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'period'], 'required'],
            [['description', 'status'], 'string'],
            [['pages_allowed', 'downloads_allowed', 'publishes_allowed', 'created', 'modified'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['period'], 'string', 'max' => 50],
            [['templateIds'], 'safe']
        ];
    }

    public function attributeLabels(){
        $labels=parent::attributeLabels();
        $labels['templatesAssigned']='Templates';
        return $labels;
    }

    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',
            ]
        ];
    }


    public function afterSave($insert, $changedAttributes)
    {
        /**
         * remove templates dependencies
         */
        $assignments = SubscriptionTemplateAssignment::find()->where("subscription_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save templates dependencies
         */
        if (!empty($this->templateIds)) {
            if (is_array($this->templateIds)) {
                foreach ($this->templateIds as $id) {
                    $r = new SubscriptionTemplateAssignment();
                    $r->subscription_id = $this->id;
                    $r->template_id = $id;
                    $r->save();
                }
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionTemplateAssignments()
    {
        return $this->hasMany(SubscriptionTemplateAssignment::className(), ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('subscription_user_assignment', ['subscription_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplatesAssigned()
    {
        return $this->hasMany(Template::className(), ['id' => 'template_id'])->viaTable('subscription_template_assignment', ['subscription_id' => 'id']);
    }


    public function getTemplatesNumber()
    {
        return SubscriptionTemplateAssignment::find()->where(['subscription_id' => $this->id])->count();
    }

    public function getEndDateForCurrentSubscriptionFromNow()
    {
        $period = $this->period;

        /** unlimited */
        if ($period == 0) $period = '10_year';

        $date = strtotime('+ ' . str_replace('_', ' ', $period));

        return $date;
    }

}