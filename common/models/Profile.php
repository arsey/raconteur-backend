<?php

namespace common\models;

use Yii;
use \yii\db\ActiveRecord;

class Profile extends \common\models\base\BaseProfile
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'company'], 'required'],
            [['sessions'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 30],
            [['company'], 'string', 'max' => 150],
            [['position'], 'string', 'max' => 100],
            [['phone'], 'string', 'max' => 45],
            [['photo'], 'integer', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        $labels=parent::attributeLabels();
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'value' => function () {
                    return date("Y-m-d H:i:s");
                },
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'create_time',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_time',
                ],
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        $user = Yii::$app->getModule("user")->model("User");
        return $this->hasOne($user::className(), ['id' => 'user_id']);
    }

    /**
     * Set user id
     *
     * @param int $userId
     * @return static
     */
    public function setUser($userId)
    {
        $this->user_id = $userId;
        return $this;
    }
}