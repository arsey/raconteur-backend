<?php

namespace common\models;

use \common\models\base\BaseTemplate;
use yii\behaviors\TimestampBehavior;
use \common\models\ScriptTemplateAssignment;
use \common\models\TemplateImageAssignment;
use \common\models\UserTemplateAssignment;
use \common\models\SubscriptionUserAssignment;
use \common\models\Subscription;
use \common\models\Section;
use \common\models\Script;
use \backend\models\Mediafile;
use \backend\models\User;
use yii\helpers\ArrayHelper;


class Template extends BaseTemplate
{
    public $scriptIds = [];
    public $imageIds = [];
    public $previewImg = null;

    public $scripts = null;

    const TYPE_FREE = 'free';
    const TYPE_PAID = 'paid';

    public static $types = [
        self::TYPE_FREE => 'Free',
        self::TYPE_PAID => 'Paid',
    ];

    const STATUS_ACTIVE = 'active';
    const STATUS_DISABLED = 'disabled';

    /** @var array $dependencies uses to extend api response on view request */
    public $dependencies = ['scripts' => [], 'images' => []];
    /** @var array $sections uses to extend api response on view request */
    public $sections = [];

    public function fields()
    {
        return [
            'id',
            'title',
            'comment',
            'preview_img',
            'html',
            'css',
            'type',
            'dependencies',
            'sections'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['id', 'created', 'modified'], 'integer'],
            [['html', 'css', 'type', 'status'], 'string'],
            [['title'], 'string', 'max' => 150],
            [['comment', 'preview_img'], 'string', 'max' => 255],
            [['scriptIds', 'imageIds'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Template title',
            'comment' => 'Comments',
            'preview_img' => 'Preview Image',
            'html' => 'HTML',
            'css' => 'CSS',
            'type' => 'Template type',
            'created' => 'Created',
            'modified' => 'Modified',
            'status' => 'Template status',
        ];
    }

    public function behaviors()
    {
        return [
            'dateTimeStampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created',
                'updatedAtAttribute' => 'modified',

            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {

        /**
         * remove scripts dependencies
         */
        $assignments = ScriptTemplateAssignment::find()->where("template_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save scripts dependencies
         */
        if (!empty($this->scriptIds)) {
            $o = 0;
            $this->scriptIds = array_unique($this->scriptIds);
            foreach ($this->scriptIds as $id) {
                $r = new ScriptTemplateAssignment();
                $r->template_id = $this->id;
                $r->script_id = $id;
                $r->order = $o;
                $r->save();
                $o++;
            }
        }

        /**
         * remove images dependencies
         */
        $assignments = TemplateImageAssignment::find()->where("template_id={$this->id}")->all();
        if ($assignments) {
            foreach ($assignments as $assignment) {
                $assignment->delete();
            }
        }

        /**
         * save images dependencies
         */
        if (!empty($this->imageIds)) {
            $this->imageIds = array_unique($this->imageIds);
            foreach ($this->imageIds as $id) {
                $tia = new TemplateImageAssignment();
                $tia->template_id = $this->id;
                $tia->file_id = $id;
                $tia->save();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptTemplateAssignments()
    {
        return $this->hasMany(ScriptTemplateAssignment::className(), ['template_id' => 'id'])->orderBy('order');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateImageAssignments()
    {
        return $this->hasMany(TemplateImageAssignment::className(), ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSectionsAssigned()
    {
        return $this->hasMany(Section::className(), ['id' => 'section_id'])->viaTable('template_section_assignment', ['template_id' => 'id']);
    }

    public function getSectionsNumber()
    {
        return TemplateSectionAssignment::find()->where(['template_id' => $this->id])->count();
    }

    public function getUserTemplateAssignmentNumber()
    {
        return UserTemplateAssignment::find()->where(['template_id' => $this->id])->count();
    }

    public function getFilesAssigned()
    {
        return $this->hasMany(Mediafile::className(), ['id' => 'file_id'])->viaTable('template_image_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScriptsAssigned()
    {
        return $this->hasMany(Script::className(), ['id' => 'script_id'])->viaTable('script_template_assignment', ['template_id' => 'id']);
    }

    public static function getUserAllowedTemplatesIds($userId)
    {
        $freeTemplates = self::getFreeTemplatesIds();
        $directlyAssignedTemplates = UserTemplateAssignment::getUserAssignedTemplatesIds($userId);

        $templatesFromSubscription = [];
        if ($userActiveSubscription = SubscriptionUserAssignment::getUserActiveSubscription($userId)) {
            if ($subscription = Subscription::find()->where(['id' => $userActiveSubscription->subscription_id])->one()) {
                $subscriptionTemplates = $subscription->subscriptionTemplateAssignments;
                if ($subscriptionTemplates) {
                    foreach ($templatesFromSubscription as $tfs) {
                        $templatesFromSubscription[] = $tfs->template_id;
                    }
                }
            }
        }

        $ids = array_merge($freeTemplates, $directlyAssignedTemplates, $templatesFromSubscription);
        return ArrayHelper::getColumn(Template::find()->where(['status' => Template::STATUS_ACTIVE, 'id' => $ids])->all(), 'id');
    }

    public function getUsersIdsAssignedToTemplate()
    {
        $users = [];

        $directlyAssignedUsers = $this->users;
        if ($directlyAssignedUsers) {
            foreach ($directlyAssignedUsers as $user) {
                $users[] = $user->id;
            }
        }

        $assignedViaSubscriptions = $this->userIdsThroughSubscription;

        $users = array_merge($users, $assignedViaSubscriptions);

        return array_unique($users);
    }

    public function getUserIdsThroughSubscription()
    {
        $usersIds = [];
        $subscriptions = $this->subscriptions;
        if ($subscriptions) {
            foreach ($subscriptions as $subscription) {
                $users = $subscription->users;
                if ($users) {
                    foreach ($users as $user) {
                        $usersIds[] = $user->id;
                    }
                }
            }
        }

        return $usersIds;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_template_assignment', ['template_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['id' => 'subscription_id'])->viaTable('subscription_template_assignment', ['template_id' => 'id']);
    }

    public static function getFreeTemplatesIds()
    {
        return self::find()->select('id')->where(['type' => self::TYPE_FREE, 'status' => self::STATUS_ACTIVE])->column();
    }

    public function getPreviewImageModel()
    {
        if ($this->preview_img && $previewImg = Mediafile::find()->where('id=:id', [':id' => $this->preview_img])->one()) {
            $this->previewImg = $previewImg;
        }

        return $this->previewImg;
    }

    public function fetchDependencies()
    {
        $this->dependencies['images'] = $this->filesAssigned;
        $this->dependencies['scripts'] = $this->scriptsAssigned;

        return $this->dependencies;
    }

    public function fetchSections()
    {
        $this->sections = $this->sectionsAssigned;
        if ($this->sections) {
            foreach ($this->sections as $key => $section) {
                if (is_numeric($section->preview_img)) {
                    $section->preview_img = $section->previewImageModel;
                    $section->fetchDependencies();
                }
            }
        }

        return $this->sections;
    }
}