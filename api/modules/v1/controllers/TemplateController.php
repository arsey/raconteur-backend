<?php

namespace api\modules\v1\controllers;

use common\models\Template;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use common\models\UserRequest;

/**
 * User Request Controller API
 */
class TemplateController extends MainApiController
{
    public $modelClass = 'common\models\Template';

    public $serializer = [
        'class' => 'api\modules\v1\components\TemplateSerializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['options'],
                'allow' => true,
                'roles' => ['?', '@']
            ],
            [
                'actions' => ['index', 'view'],
                'allow' => true,
                'roles' => ['@']
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // unset standard actions to override them in this controller
        unset($actions['index']);

        return $actions;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if ($action === 'view') {
            $allowedTemplates = Template::getUserAllowedTemplatesIds(Yii::$app->user->id);
            if (!in_array($model->id, $allowedTemplates))
                throw new ForbiddenHttpException("Access Denied");
        }
    }

    public function actionIndex()
    {
        $modelClass = $this->modelClass;
        $allowedTemplates = Template::getUserAllowedTemplatesIds(Yii::$app->user->id);
        return new \yii\data\ActiveDataProvider([
            'query' => $modelClass::find()->where(['id' => $allowedTemplates])
        ]);
    }
}