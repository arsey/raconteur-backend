<?php

namespace api\modules\v1\controllers;

use common\components\PageManager;
use common\models\SubscriptionUserAssignment;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use common\models\Page;

class PageController extends MainApiController
{
    public $modelClass = 'common\models\Page';
    protected $currentUserSubscription = null;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['options'],
                'allow' => true,
                'roles' => ['?', '@']
            ],
            [
                'actions' => ['index', 'view', 'create', 'update', 'delete', 'publish', 'download', 'publish'],
                'allow' => true,
                'roles' => ['@']
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // unset standard actions to override them in this controller
        unset($actions['create'], $actions['update'], $actions['delete'], $actions['view']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if ($action->id == 'options')
            return true;

        $this->currentUserSubscription = SubscriptionUserAssignment::getUserActiveSubscription(Yii::$app->user->id);
        if (!$this->currentUserSubscription) {
            throw new ForbiddenHttpException("You don't have active subscription to use this functionality. Please contact to administrator with this issue.");
        }

        return true;
    }

    public function actionView($id)
    {
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);
        if (!$model)
            throw new NotFoundHttpException('Page with given ID was not found');

        $this->checkAccess($this->action, $model);

        return $model;
    }

    public function actionCreate()
    {
        $model = new $this->modelClass();

        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        /** load data into model from request */
        $model->load($bodyParams, '');

        $savedPages = Page::getUserNumberSavedPages(Yii::$app->user->id);
        if ($this->currentUserSubscription->subscription->pages_allowed <= $savedPages)
            throw new ForbiddenHttpException('Limit exceed');

        /** define some strict fields */
        $model->user_id = Yii::$app->user->id;
        $model->url_name = null;
        $model->status = Page::STATUS_UNPUBLISHED;

        if ($model->validate()) {
            $model->save();

            $this->currentUserSubscription->pages_saved = $savedPages + 1;
            $this->currentUserSubscription->save();

            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function actionUpdate($id)
    {
        /* @var $model ActiveRecord */
        $model = $this->findModel($id);
        if (!$model)
            throw new NotFoundHttpException('Page with given ID was not found');

        $this->checkAccess($this->action, $model);

        $request = Yii::$app->getRequest();
        $model->template_id = $request->getBodyParam('template_id', $model->template_id);
        $model->title = $request->getBodyParam('title', $model->title);
        $model->content = $request->getBodyParam('content', $model->content);
        $model->header = $request->getBodyParam('header', $model->header);
        $model->navigation = $request->getBodyParam('navigation', $model->navigation);
        $model->html = $request->getBodyParam('html', $model->html);

        if ($model->save() === false && !$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
        }

        return $model;
    }

    public function actionDelete($id)
    {

        $model = $this->findModel($id);
        if (!$model)
            throw new NotFoundHttpException('Page with given ID was not found');

        $this->checkAccess($this->action, $model);

        $model->status = Page::STATUS_REMOVED;
        if ($model->save() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        $savedPages = Page::getUserNumberSavedPages(Yii::$app->user->id);
        $this->currentUserSubscription->pages_saved = $savedPages;
        $this->currentUserSubscription->save();

        Yii::$app->getResponse()->setStatusCode(204);
    }

    public function prepareDataProvider($action)
    {
        $modelClass = $action->modelClass;

        $allowedStatuses = [Page::STATUS_PUBLISHED, Page::STATUS_UNPUBLISHED];

        $request = Yii::$app->request;
        if ($status = $request->getQueryParam('status', 'null')) {
            if (in_array($status, $allowedStatuses)) {
                $allowedStatuses = $status;
            }
        }

        $where = ['user_id' => Yii::$app->user->id, 'status' => $allowedStatuses];

        return new ActiveDataProvider(['query' => $modelClass::find()->where($where)]);
    }

    public function actionDownload($id)
    {
        $downloads = $this->currentUserSubscription->downloads;
        if ($this->currentUserSubscription->subscription->downloads_allowed <= $downloads)
            throw new ForbiddenHttpException('Limit exceed');

        $model = $this->findModel($id);
        if ($model) {
            $suf = '/uploads/archived-page/';

            $archivedPagesDir = Yii::getAlias('@webroot' . $suf);

            if (!is_dir($archivedPagesDir))
                mkdir($archivedPagesDir, 0777, true);

            $filename = uniqid() . '.zip';

            $destination = $archivedPagesDir . $filename;

            $pageMngr = Yii::$app->page;
            if ($pageMngr->make($model->html)->zip($destination)) {
                $pageMngr->deleteTempDir();


                $this->currentUserSubscription->downloads = $downloads + 1;
                $this->currentUserSubscription->save();

                return ['link' => $suf . $filename];
            }
        }
        throw new NotFoundHttpException('Page with given ID was not found');
    }

    public function actionPublish($id)
    {
        $publishedPages = Page::getUserNumberPublishedPages(Yii::$app->user->id);
        if ($this->currentUserSubscription->subscription->publishes_allowed <= $publishedPages)
            throw new ForbiddenHttpException('Limit exceed');

        $model = $this->findModel($id);
        if ($model) {
            $username = Yii::$app->user->identity->username;
            $request = Yii::$app->getRequest();
            $sluggifiedUrl = PageManager::sluggify(PageManager::sanitizeWithTranslit($request->getBodyParam('url', uniqid())));
            $dirName = $username . DIRECTORY_SEPARATOR . $sluggifiedUrl;

            $suf = '/pub/' . $dirName;
            $pubDir = Yii::getAlias('@mainRootWeb' . $suf);

            if (!is_dir($pubDir))
                mkdir($pubDir, 0777, true);
            else
                throw new ForbiddenHttpException("Page '$sluggifiedUrl' already exists");

            Yii::$app->page
                ->make($model->html)
                ->publish($pubDir)
                ->deleteTempDir();

            if ($model->url_name) {
                $oldPubDir = str_replace('index.html', '', Yii::getAlias('@mainRootWeb' . $model->url_name));
                PageManager::rrmdir($oldPubDir);
            }

            $pageUrl = $suf . '/index.html';

            $model->url_name = $pageUrl;
            $model->status = Page::STATUS_PUBLISHED;
            $model->save(false);

            $this->currentUserSubscription->pages_published = $publishedPages + 1;
            $this->currentUserSubscription->save();

            return ['link' => $pageUrl];
        }
        throw new NotFoundHttpException('Page with given ID was not found');
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if (($action === 'update' || $action === 'view' || $action === 'delete') && $model->user_id !== Yii::$app->user->id)
            throw new ForbiddenHttpException("Access Denied");
    }

    protected function findModel($id)
    {
        return Page::find()
            ->where(['id' => $id, 'user_id' => Yii::$app->user->id, 'status' => [Page::STATUS_PUBLISHED, Page::STATUS_UNPUBLISHED]])
            ->one();
    }
}