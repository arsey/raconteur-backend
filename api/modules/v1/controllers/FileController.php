<?php

namespace api\modules\v1\controllers;

use Yii;
use api\modules\v1\models\Mediafile;

/**
 * File Controller API
 */
class FileController extends MainApiController
{
    public $modelClass = 'api\modules\v1\models\Mediafile';

    public $serializer = [
        'class' => 'api\modules\v1\components\FileSerializer',
        'collectionEnvelope' => 'items',
    ];

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['options'],
                'allow' => true,
                'roles' => ['?', '@']
            ],
            ['actions' => ['index', 'dummy', 'create', 'view', 'update', 'delete'],
                'allow' => true,
                'roles' => ['@']
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // unset standard actions to override them in this controller
        unset($actions['create'], $actions['index'], $actions['view'], $actions['update'], $actions['delete']);

        return $actions;
    }

    public function actionIndex()
    {
        $modelClass = $this->modelClass;
        return new \yii\data\ActiveDataProvider([
            'query' => $modelClass::find()
                ->joinWith([
                    'owners' => function ($query) {
                        $query
                            ->where(['owner_id' => Yii::$app->user->id])
                            ->andWhere(['owner_attribute' => Mediafile::OWNER_ATTRIBUTE_FILE_FROM_THIRD_APP]);
                    }
                ])
        ]);
    }

    public function actionView($id)
    {
        if ($file = $this->getUserFileById($id))
            return $file;

        throw new \yii\web\NotFoundHttpException('File with id "' . $id . '" was not found');
    }

    public function actionCreate()
    {
        $fileModule = Yii::$app->getModule('filemanager');

        $model = new Mediafile();
        if ($model->saveUploadedFile($fileModule->routes)) {
            $model->addOwner(Yii::$app->user->id, Yii::$app->user->identity->email, Mediafile::OWNER_ATTRIBUTE_FILE_FROM_THIRD_APP);
            $model->createThumbs($fileModule->routes, $fileModule->thumbs);
        }

        return $model;
    }

    public function actionUpdate($id)
    {
        $file = $this->getUserFileById($id);

        if (!$file)
            throw new \yii\web\NotFoundHttpException('File with id "' . $id . '" was not found');

        $request = Yii::$app->request;

        $file->alt = $request->post('alt', $file->alt);
        $file->description = $request->post('description', $file->description);

        if ($file->validate()) {
            $file->save(false);
        }

        return $file;
    }

    public function actionDelete($id)
    {
        $file = $this->getUserFileById($id);

        if (!$file)
            throw new \yii\web\NotFoundHttpException('File with id "' . $id . '" was not found');

        $fileModule = Yii::$app->getModule('filemanager');

        $file->deleteThumbs($fileModule->routes);
        $file->deleteFile($fileModule->routes);

        if ($file->delete() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }

    private function getUserFileById($id)
    {
        $modelClass = $this->modelClass;

        return $modelClass::find()
            ->joinWith([
                'owners' => function ($query) {
                    $query
                        ->where(['owner_id' => Yii::$app->user->id])
                        ->andWhere(['owner_attribute' => Mediafile::OWNER_ATTRIBUTE_FILE_FROM_THIRD_APP]);
                }
            ])
            ->where(['id' => $id])
            ->one();
    }
}