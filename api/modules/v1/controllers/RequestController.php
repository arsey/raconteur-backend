<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\ServerErrorHttpException;
use common\models\UserRequest;

/**
 * User Request Controller API
 */
class RequestController extends MainApiController
{
    public $modelClass = 'common\models\UserRequest';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['options'],
                'allow' => true,
                'roles' => ['?', '@']
            ],
            [
                'actions' => ['index', 'options', 'create', 'view', 'update'],
                'allow' => true,
                'roles' => ['@']
            ]
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        // unset standard actions to override them in this controller
        unset($actions['create']);

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }


    /**
     * Create action implements the API endpoint for creating a new user request model from the given data.
     * @return mixed
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        $model = new $this->modelClass();

        /** load data into model from request */
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');

        /** define some strict fields */
        $model->user_id = Yii::$app->user->id;
        $model->status = UserRequest::STATUS_PENDING;

        if ($model->save()) {

            //TODO  for a new template we really just need an email to come to us with username, businessname, user-contact-details, subject=new template request
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute(['view', 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }

    public function prepareDataProvider($action)
    {
        $modelClass = $action->modelClass;
        return new ActiveDataProvider([
            'query' => $modelClass::find()->where('user_id=:userId', [':userId' => Yii::$app->user->id]),
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if (($action === 'update' || $action === 'view') && $model->user_id !== Yii::$app->user->id)
            throw new ForbiddenHttpException("Access Denied");
    }
}