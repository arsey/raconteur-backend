<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\filters\AccessControl;
use api\modules\v1\models\Mediafile;
use common\models\SubscriptionUserAssignment;

class UserController extends MainApiController
{

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors ['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['options', 'forgot', 'reset', 'auth', 'deauth'],
                    'allow' => true,
                    'roles' => ['?', '@']
                ],
                [
                    'actions' => ['profile', 'update-profile', 'update-photo'],
                    'allow' => true,
                    'roles' => ['@']
                ]
            ]
        ];
        return $behaviors;
    }

    public function actionAuth()
    {
        $model = Yii::$app->getModule("user")->model("LoginForm");
        $model->rememberMe = true;

        $requestData = ['LoginForm' => Yii::$app->request->post()];
        $loginDuration = Yii::$app->getModule("user")->loginDuration;

        if ($model->load($requestData) && $model->login($loginDuration)) {
            $profile = Yii::$app->user->identity->profile;
            $profile->sessions = $profile->sessions + 1;
            $profile->save();
            return ['access-token' => Yii::$app->session->id];
        }

        $model->validate();
        return $model;
    }

    public function actionDeauth()
    {
        Yii::$app->user->logout();
    }

    public function actionProfile()
    {
        return $this->getProfileInfo();
    }

    public function actionUpdateProfile()
    {
        $requestData = ['Profile' => Yii::$app->request->post()];

        $profile = Yii::$app->user->identity->profile;
        $loadedPost = $profile->load($requestData);

        //validate
        if ($loadedPost && $profile->validate()) {
            $profile->save(false);
            return $this->getProfileInfo();
        }

        return $profile;
    }

    public function actionUpdatePhoto()
    {
        $fileModule = Yii::$app->getModule('filemanager');

        $model = new Mediafile();
        $model->saveUploadedFile($fileModule->routes);

        /**
         * create thumbnails
         */
        $model->createThumbs($fileModule->routes, $fileModule->thumbs);

        $model->addOwner(Yii::$app->user->id, Yii::$app->user->identity->email, 'user_avatar');

        /**
         * update user profile
         */
        $userProfile = Yii::$app->user->identity->profile;
        $userProfile->photo = $model->id;
        $userProfile->save(false);

        $profileInfo = $this->getProfileInfo();

        return $profileInfo;
    }

    public function actionForgot()
    {
        $requestData = ['ForgotForm' => Yii::$app->request->post()];
        $model = Yii::$app->getModule("user")->model("ForgotForm");

        //TODO:: check with a real smtp server
        if ($model->load($requestData) && $model->sendForgotEmail()) {
            return ['message' => 'Email with reset code was sent to your email'];
        }

        return $model;
    }

    public function actionReset()
    {
        $user = Yii::$app->getModule("user")->model("User");

        $requestData = Yii::$app->request->post();

        $userKey = Yii::$app->getModule("user")->model("UserKey");
        $userKey = $userKey::findActiveByKey(@$requestData['key'], $userKey::TYPE_PASSWORD_RESET);

        if (!$userKey) {
            $user->addError('key', 'Invalid key(You could get it via email right after the "forgot" step)');
            return $user;
        }

        // get user and set "reset" scenario
        $user = $user::findOne($userKey->user_id);
        $user->setScenario("reset");

        // load post data and reset user password
        if (isset($requestData['new_password'])) {
            $requestData['newPassword'] = $requestData['new_password'];
            unset($requestData['new_password']);
        }

        if (isset($requestData['new_password_confirm'])) {
            $requestData['newPasswordConfirm'] = $requestData['new_password_confirm'];
            unset($requestData['new_password_confirm']);
        }

        if ($user->load(['User' => $requestData]) && $user->save()) {
            // consume userKey and set success = true
            $userKey->consume();

            return ['success' => true];
        }

        return $user;
    }

    private function getProfileInfo()
    {
        $user = Yii::$app->user->identity;
        $profile = $user->profile;

        $fieldsToSend = [
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name,
            'company' => $profile->company,
            'position' => $profile->position,
            'login_time' => strtotime($user->attributes['login_time']),
            'email' => $user->attributes['email'],
        ];


        if ($currentUserSubscription = SubscriptionUserAssignment::getUserActiveSubscription($user->id)) {
            $attrs = $currentUserSubscription->attributes;
            unset($attrs['id']);

            $subscription=$currentUserSubscription->subscription;
            if($subscription){
                $attrs['name']=$subscription->name;
                $attrs['description']=$subscription->description;
                $attrs['pages_allowed']=$subscription->pages_allowed;
                $attrs['publishes_allowed']=$subscription->publishes_allowed;
                $attrs['downloads_allowed']=$subscription->downloads_allowed;
                $attrs['price']=$subscription->price;
            }

            $fieldsToSend['subscription'] = $attrs;
        }

        if (is_numeric($profile->photo)) {
            $photo = Mediafile::findOne($profile->photo);
            if ($photo) {
                $photoAttrs = $photo->attributes;

                unset($photoAttrs['updated_at'], $photoAttrs['description'], $photoAttrs['alt']);
                $fieldsToSend['photo'] = $photoAttrs;
            }
        }

        return $fieldsToSend;
    }
}