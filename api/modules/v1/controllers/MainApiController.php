<?php
namespace api\modules\v1\controllers;

use yii;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\AccessControl;

class MainApiController extends ActiveController
{
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    public $modelClass = false;

    public function actionIndex()
    {
        return $this->redirect('http://docs-raconteur.loc/');
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => 'yii\filters\ContentNegotiator',
            'formats' => [
                'text/html' => Response::FORMAT_JSON,
                'application/json' => Response::FORMAT_JSON,
                'application/xml' => Response::FORMAT_XML,
            ],
        ];
        $behaviors ['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['dummy'],
                    'allow' => true,
                    'roles' => ['?']
                ]
            ],
            'denyCallback' => function () {
                throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
            }
        ];
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
                'Access-Control-Allow-Credentials' => true,
                'Access-Control-Max-Age' => 86400,
            ],
        ];
        return $behaviors;
    }

    /**
     *  Overridden init method  to allow api controllers without model
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->modelClass !== false && $this->modelClass === null) {
            throw new \yii\base\InvalidConfigException('The "modelClass" property must be set.');
        }
    }

    public function actionDummy()
    {
        return array('status' => 'ok');
    }
}