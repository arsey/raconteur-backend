<?php

namespace api\modules\v1\components;

use yii\rest\Serializer;

class TemplateSerializer extends Serializer
{
    /**
     * Serializes a set of models.
     * @param array $models
     * @return array the array representation of the models
     */
    protected function serializeModels(array $models)
    {
        list ($fields, $expand) = $this->getRequestedFields();
        foreach ($models as $i => $model) {

            if (is_numeric($model->preview_img)) {
                    $model->preview_img=$model->previewImageModel;
            }

            if ($model instanceof Arrayable) {
                $models[$i] = $model->toArray($fields, $expand);
            } elseif (is_array($model)) {
                $models[$i] = ArrayHelper::toArray($model);
            }
        }

        return $models;
    }


    /**
     * Serializes a model object.
     * @param Arrayable $model
     * @return array the array representation of the model
     */
    protected function serializeModel($model)
    {
        if ($this->request->getIsHead()) {
            return null;
        } else {
            list ($fields, $expand) = $this->getRequestedFields();

            if (is_numeric($model->preview_img)) {
                $model->preview_img=$model->previewImageModel;
            }

            $model->fetchDependencies();

            $model->fetchSections();

            return $model->toArray($fields, $expand);
        }
    }
}