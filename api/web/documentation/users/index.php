<?php

require dirname(__FILE__) . '/../docsettings.php';
header('Content-type: application/json');
echo '{
    "apiVersion": "0.2",
    "swaggerVersion": "1.1",
    "basePath": "' . API_HOST . '",
    "resourcePath": "/users",
    "apis":[';
echo file_get_contents('auth.json');
echo ',';
echo file_get_contents('de_auth.json');
echo ',';
echo file_get_contents('profile.json');
echo ',';
echo file_get_contents('update_profile.json');
echo ',';
echo file_get_contents('update_photo.json');
echo ',';
echo file_get_contents('forgot.json');
echo ',';
echo file_get_contents('reset.json');
echo '],';
echo file_get_contents('models.json');
echo '}';

