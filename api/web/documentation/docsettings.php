<?php

$sn = $_SERVER['SERVER_NAME'];
$host = '';

if ($sn === 'api-raconteur.arsey.net') {
    $host = 'api-raconteur.arsey.net/v1';
} else if ($sn === 'api-raconteur.loc') {
    $host = 'api-raconteur.loc/v1';
}

define('API_HOST', 'http://' . $host);