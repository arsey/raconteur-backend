<!DOCTYPE html>
<html>
<head>
    <title>Raconteur API Documentation</title>

    <link rel="shortcut icon" type="image/x-icon"  href="/favicon.ico">

    <link href='css/reset.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='css/screen.css' media='screen' rel='stylesheet' type='text/css'/>
    <link href='css/reset.css' media='print' rel='stylesheet' type='text/css'/>
    <link href='css/screen.css' media='print' rel='stylesheet' type='text/css'/>
    <script type="text/javascript" src="lib/shred.bundle.js"></script>
    <script src='lib/jquery-1.8.0.min.js' type='text/javascript'></script>
    <script src='lib/jquery.slideto.min.js' type='text/javascript'></script>
    <script src='lib/jquery.wiggle.min.js' type='text/javascript'></script>
    <script src='lib/jquery.ba-bbq.min.js' type='text/javascript'></script>
    <script src='lib/handlebars-1.0.0.js' type='text/javascript'></script>
    <script src='lib/underscore-min.js' type='text/javascript'></script>
    <script src='lib/backbone-min.js' type='text/javascript'></script>
    <script src='lib/swagger.js' type='text/javascript'></script>
    <script src='swagger-ui.js' type='text/javascript'></script>
    <script src='lib/highlight.7.3.pack.js' type='text/javascript'></script>

    <script type="text/javascript">
        $(function () {
            $.ajaxSetup({xhrFields: {withCredentials: true}});

            window.swaggerUi = new SwaggerUi({
                url: "http://<?php echo $_SERVER['SERVER_NAME']; ?>/documentation/resources.php",
                dom_id: "swagger-ui-container",
                supportHeaderParams: false,
                supportedSubmitMethods: ['get', 'post', 'put', 'delete','options'],
                onComplete: function (swaggerApi, swaggerUi) {
                    $('pre code').each(function (i, e) {
                        hljs.highlightBlock(e)
                    });
                },
                onFailure: function (data) {
                    log("Unable to Load SwaggerUI");
                    console.log("Unable to Load SwaggerUI");
                    console.log(data);
                },
                docExpansion: "none"
            });

            window.swaggerUi.load();
        });
    </script>
</head>
<body class="swagger-section">

<div id='header'>
    <div class="swagger-ui-wrap">
        <h1>Raconteur API Documentation</h1>
    </div>
</div>
<div id="message-bar" class="swagger-ui-wrap">&nbsp;</div>
<div id="swagger-ui-container" class="swagger-ui-wrap">
</div>
</body>
</html>