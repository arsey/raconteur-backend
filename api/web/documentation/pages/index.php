<?php

require dirname(__FILE__) . '/../docsettings.php';
header('Content-type: application/json');
echo '{
    "apiVersion": "0.2",
    "swaggerVersion": "1.1",
    "basePath": "' . API_HOST . '",
    "resourcePath": "/pages",
    "apis":[';
echo file_get_contents('list.json');
echo ',';
echo file_get_contents('view.json');
echo ',';
echo file_get_contents('download.json');
echo ',';
echo file_get_contents('publish.json');
echo ',';
echo file_get_contents('create.json');
echo ',';
echo file_get_contents('update.json');
echo ',';
echo file_get_contents('delete.json');
echo '],';
echo file_get_contents('models.json');
echo '}';