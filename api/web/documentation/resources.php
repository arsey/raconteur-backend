<?php

header("Content-type: application/json");
echo '{
  "apiVersion": "1.0",
  "swaggerVersion": "1.0",
  "basePath": "http://' . $_SERVER['SERVER_NAME'] . '/documentation/",
  "apis": [
    {"path":"/users"},
    {"path": "/requests"},
    {"path": "/files"},
    {"path": "/templates"},
    {"path": "/pages"}
  ]
}';