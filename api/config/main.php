<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ],
        'user' => [
            'class' => 'amnah\yii2\user\Module',
            'modelClasses' => [
                'Profile' => 'common\models\Profile',
            ],
            'emailViewPath'=>'@app/modules/v1/mail'
        ],
        'filemanager' => [
            'class' => 'pendalf89\filemanager\Module',
            // Upload routes
            'routes' => [
                // Base absolute path to web directory
                'baseUrl' => '',
                // Base web directory url
                'basePath' => '@mainRootWeb',
                // Path for uploaded files in web directory
                'uploadPath' => 'uploads',
            ],
            // Thumbnails info
            'thumbs' => [
                'small' => [
                    'name' => 'Small',
                    'size' => [100, 100],
                ],
                'medium' => [
                    'name' => 'Medium',
                    'size' => [300, 200],
                ],
                'large' => [
                    'name' => 'Large',
                    'size' => [500, 400],
                ],
            ],
        ],
    ],
    'components' => [
        'user' => [
            'class' => 'amnah\yii2\user\components\User'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'page'=>[
          'class'=>'common\components\PageManager'
        ],
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'cookieValidationKey' => '4DT96bnc5YqSnHDNP0xTachk8eiJtMal',
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'format' => 'json',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if ($response->data !== null && isset($response->data['code']) && isset($response->data['status']) && isset($response->data['type'])) {
                    unset($response->data['code'], $response->data['type']);
                }
            }
        ],
        'session' => array(
            'name' => 'auth-token',
            'class' => 'yii\web\DbSession',
            'timeout' => 30 * 3600 * 24,
        ),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                //USERS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/user',
                    'extraPatterns' => [
                        'POST auth' => 'auth',
                        'OPTIONS auth' => 'options',

                        'POST deauth' => 'deauth',
                        'OPTIONS deauth' => 'options',

                        'GET profile' => 'profile',
                        'OPTIONS ' => 'options',

                        'POST forgot' => 'forgot',
                        'OPTIONS forgot' => 'options',

                        'POST reset' => 'reset',
                        'OPTIONS reset' => 'options',

                        'PUT profile' => 'update-profile',
                        'OPTIONS profile' => 'options',

                        'POST profile/photo' => 'update-photo',
                        'OPTIONS profile/photo' => 'options',
                    ]
                ],
                //REQUESTS
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/request',
                    'except' => ['delete']
                ],
                //FILES
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/file'
                ],
                //TEMPLATES
                [
                    'class' => '\yii\rest\UrlRule',
                    'controller' => 'v1/template',
                    'except' => ['delete', 'update', 'create']
                ],
                //PAGES
                [
                    'class'=>'\yii\rest\UrlRule',
                    'controller'=>'v1/page',
                    'extraPatterns'=>[
                        'GET download/<id:\\d[\\d,]*>' => 'download',
                        'OPTIONS download/<id:\\d[\\d,]*>' => 'options',

                        'POST publish/<id:\\d[\\d,]*>' => 'publish',
                        'OPTIONS publish/<id:\\d[\\d,]*>' => 'options',
                    ]
                ]
            ],
        ]
    ],
    'params' => $params,
];