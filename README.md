## Requirements

You must install [Vagrant](http://vagrantup.com), [VirtualBox](https://www.virtualbox.org) and few vagrant plugins:

    $ vagrant plugin install vagrant-librarian-chef-nochef vagrant-omnibus

## How to start

    $ cd <project_root>
    $ vagrant up

## SSH access

You can login on VM over SSH with these ways:

* As administrator: `$ vagrant ssh`

## Remote debug with Xdebug (using PhpStorm or IDEA) - [official instructions](https://www.jetbrains.com/phpstorm/help/configuring-xdebug.html)

To debug application inside VM you need to add PHP server (button "Add" in "File - Settings - PHP - Servers") with the following settings:

* **Name**: raconteur.loc
* **Host**: raconteur.loc
* **Port**: 80
* **Debugger**: Xdebug
* **Use path mappings**: On (project folder in VM must correspond to ["php-app"]["project_dir"] in json-file)

Then you need to create configuration with type "PHP Web application" (in "Run - Edit configurations") using created PHP server.