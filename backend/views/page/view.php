<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Pages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view">

    <iframe src="/page/html?id=<?= $model->id ?>"
            style="width: 100%;height: 545px;border: 1px solid #F5F5F5;  border-radius:0px;  box-shadow: 0px 0px 5px #ACACAC;  margin-bottom: 30px;margin-top:-15px"></iframe>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'template_id',
            'url_name:url',
            'status',
        ],
    ]) ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
</div>