<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Page', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            'title',
            [
                'attribute' => 'username',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->user->username, ['/admin/view', 'id' => $data->user->id]);
                },
                'headerOptions' => ['style' => 'width: 150px;'],
            ],
            [
                'attribute' => 'template',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::a($data->template->title, ['/template/view', 'id' => $data->template->id]);
                },
                'headerOptions' => ['style' => 'width: 250px;'],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
