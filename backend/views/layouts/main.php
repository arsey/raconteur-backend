<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <link rel="shortcut icon" type="image/x-icon" href="/raconteurFav.ico">

    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="wrap">
    <?php

    NavBar::begin([
        'brandLabel' => 'Raconteur Admin',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => ['class' => 'navbar-inverse navbar-fixed-top']
    ]);


    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            [
                'label' => 'Users',
                'items' => [
                    ['label' => 'Accounts', 'url' => ['/user/admin']],
                    ['label' => 'Requests', 'url' => ['/request/index']],
                    ['label' => 'Subscriptions', 'url' => ['/subscription/index']],
                    ['label'=>'File Manager', 'url' => '/filemanager/default/index'],
                ]
            ],
            [
                'label' => 'Landing Pages',
                'items' => [
                    ['label' => 'Templates', 'url' => ['/template/index']],
                    ['label' => 'Sections', 'url' => ['/section/index']],
                    ['label' => 'Pages', 'url' => ['/page/index']],
                    ['label' => 'Scripts', 'url' => ['/script/index']]
                ]
            ],
            [
                'label' => 'Me',
                'items' => [
                    ['label' => 'Account', 'url' => ['/user/account']],
                    ['label' => 'Profile', 'url' => ['/user/profile']],
                ]
            ],
            Yii::$app->user->isGuest ?
                ['label' => 'Login', 'url' => ['/user/login']] :
                ['label' => 'Logout (' . Yii::$app->user->identity->username . ')', 'url' => ['/user/logout'], 'linkOptions' => ['data-method' => 'post']],
        ],
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
