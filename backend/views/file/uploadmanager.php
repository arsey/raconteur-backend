<?php
use dosamigos\fileupload\FileUploadUI;
use pendalf89\filemanager\Module;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel pendalf89\filemanager\models\Mediafile */

?>

<header id="header">
    <span class="glyphicon glyphicon-upload">
    </span> <?= Module::t('main', 'Upload manager') ?>
</header>

<div id="uploadmanager">
    <p>
        <?php
        $ownerAttribute = Yii::$app->request->get('owner_attribute', null);
        $ownerId = Yii::$app->request->get('owner_id', null);

        $linkUrl = ['file/filemanager'];
        $uploadUrl = ['upload'];
        if ($ownerAttribute) {
            $linkUrl['owner_attribute'] = $ownerAttribute;
            $uploadUrl['owner_attribute'] = $ownerAttribute;
        }

        if ($ownerId) {
            $linkUrl['owner_id'] = $ownerId;
            $uploadUrl['owner_id'] = $ownerId;
        }
        ?>
        <?= Html::a('← ' . Module::t('main', 'Back to file manager'), $linkUrl) ?>
    </p>

    <?php
    $fieldOptions = [];
    if ($ownerAttribute && isset($model->allowedMimes[$ownerAttribute])) {
        $fieldOptions['accept'] = implode(',', $model->allowedMimes[$ownerAttribute]);
    }

    ?>
    <?= FileUploadUI::widget([
        'model' => $model,
        'attribute' => 'file',
        'fieldOptions' => $fieldOptions,
        'url' => $uploadUrl,
        'gallery' => false,
    ]) ?>
</div>