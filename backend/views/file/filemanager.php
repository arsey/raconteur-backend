<?php

use pendalf89\filemanager\assets\FilemanagerAsset;
use pendalf89\filemanager\Module;
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel pendalf89\filemanager\models\Mediafile */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['moduleBundle'] = FilemanagerAsset::register($this);
?>

<header id="header">
    <span class="glyphicon glyphicon-picture">
    </span> <?= Module::t('main', 'File manager') ?>
</header>

<div id="filemanager" data-url-info="<?= Url::to(['file/info']) ?>">

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '<div class="items">{items}</div>{pager}',
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index, $widget) {
            return Html::a(
                Html::img($model->getDefaultThumbUrl($this->params['moduleBundle']->baseUrl))
                . '<span class="checked glyphicon glyphicon-check"></span>'
                . '<span class="filename">' . $model->filename . '</span>',
                '#mediafile',
                ['data-key' => $key]
            );
        },
    ]) ?>

    <div class="dashboard">
        <p>
            <?php
            $linkUrl = ['file/uploadmanager'];

            $ownerAttribute = Yii::$app->request->get('owner_attribute', null);
            $ownerId = Yii::$app->request->get('owner_id', null);
            if ($ownerAttribute) {
                $linkUrl['owner_attribute'] = $ownerAttribute;
            }

            if ($ownerId) {
                $linkUrl['owner_id'] = $ownerId;
            }
            ?>
            <?= Html::a('<span class="glyphicon glyphicon-upload"></span> ' . Module::t('main', 'Upload manager'), $linkUrl, ['class' => 'btn btn-default']) ?>
        </p>

        <div id="fileinfo"></div>
    </div>
</div>