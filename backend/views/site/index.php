<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
$this->title = 'Raconteur';
?>
<div class="site-index">

    <div class="jumbotron">
        <p>
            <a class="btn btn-lg btn-success" href="<?= Url::to(['/template'])?>">Manage Templates</a>
        </p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Users</h2>

                <p>Manage your users.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['user/admin']);?>">Raconteur Users &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Subscriptions</h2>
                <p>Manage subscriptions, and assign them to users.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['/subscription'])?>">Raconteur Subscriptions &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Scripts</h2>

                <p>Manage scripts and use them in templates.</p>

                <p><a class="btn btn-default" href="<?= Url::to(['/script'])?>">Raconteur Scripts &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
