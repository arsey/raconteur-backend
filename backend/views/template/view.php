<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Menu;

use backend\assets\HighlightjsAsset;

HighlightjsAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\models\Template */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="template-view">
    <div class="container">
        <div class="row">

            <div class="col-lg-4">
                <h4><?= $model->title; ?></h4>
                <figure class="default-template-preview">
                    <?php if ($model->preview_img) {
                        echo Html::img($model->previewImg->thumbs['medium']);
                    } ?>
                </figure>
            </div>

            <div class="col-lg-6">
                <section class="row">
                    <h4>Type: <?= $model->type; ?></h4>

                </section>
                <section class="row">
                    <h4>Comments</h4>
                    <?= $model->comment; ?>
                </section>
                <section class="row">
                    <h4>HTML</h4>
                    <pre><code class="html"><?= $model->html; ?></code></pre>
                </section>
                <section class="row">
                    <h4>CSS</h4>
                    <pre><code class="css"><?= $model->css; ?></code></pre>
                </section>
                <section class="row">
                    <h4>Scripts</h4>
                    <ul>
                        <?php
                        if ($scriptsAssigned = $model->scriptTemplateAssignments) {
                            foreach ($scriptsAssigned as $sa) {
                                ?>
                                <li>
                                    <?= $model->scripts[$sa->script_id] ?>
                                </li>
                            <?php
                            }
                        }
                        ?>
                    </ul>
                </section>
            </div>
        </div>

        <div class="section-to-template">

            <?php $sectionAssigned = $model->sectionsAssigned; ?>
            <h2>
                Sections: <?= $model->sectionsNumber ?>&nbsp;
                <?= Html::a('Manage sections', ['/section/index', 'SectionSearch[template_id]' => $model->id], ['class' => 'btn btn-warning btn-xs']) ?>
            </h2>

            <ul class="col-lg-8 simple-list" id="section-assignments">
                <?php
                if ($sectionAssigned = $model->sectionsAssigned) {
                    foreach ($sectionAssigned as $ia) {
                        ?>
                        <li>
                            <div class="row">
                                <div class="col-lg-1">
                                    <figure class="list-image-preview">
                                        <?php if ($ia->previewImageModel): ?>
                                            <?= Html::img($ia->previewImageModel->defaultThumbUrl); ?>
                                        <?php endif ?>
                                    </figure>
                                </div>
                                <div class="col-lg-5"><?= Html::a($ia['title'], ['/section/update', 'id' => $ia['id']]); ?> </div>
                            </div>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>

        <br/>
        <br/>
        <br/>

        <div class="row  updated">
            <div class="col-lg-12">Last updated : <?= date("F j, Y H:i", $model->modified); ?></div>
        </div>
        <div class="row updated">
            <div class="col-lg-12">
                Assigned to users : <?= $model->userTemplateAssignmentNumber; ?>&nbsp;
                <?= Html::a('Manage users', ['/user/admin', 'UserSearch[ids]' => $model->usersIdsAssignedToTemplate], ['class' => 'btn btn-warning btn-xs']) ?>
            </div>
        </div>


    </div>

    <br/>


    <div class="form-group clearfix">
        <div class="col-xs-12">
            <?= Html::a('Back to templates', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Edit template', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
</div>