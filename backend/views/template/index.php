<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TemplateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Templates';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="template-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Template', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            'title',
            'status',
            [
                'attribute' => 'sectionsNumber',
                'header' => 'Sections',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            [
                'format' => 'image',
                'value' => function ($data) {
                    if ($image = $data->previewImageModel) {
                        return $image->defaultThumbUrl;
                    }
                },
                'header' => 'Image Preview',
                'headerOptions' => ['style' => 'width: 128px;'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width: 70px;'],
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $confirmMsg = Yii::t('yii', 'Are you sure you want to delete this item?');

                        $subscriptions = count($model->subscriptions);
                        $assignedToUsers = count($model->users);
                        $assignedSections = $model->sectionsNumber;

                        if ($subscriptions + $assignedToUsers + $assignedSections > 0) {
                            $url = '#';
                            $confirmMsg = "Sorry, this template can't be deleted because: \r\n";

                            if($subscriptions>0){
                                $confirmMsg.="\r\nIt's using by $subscriptions subscription(s)";
                            }

                            if($assignedToUsers>0){
                                $confirmMsg.="\r\nIt's assigned to $assignedToUsers user(s)";
                            }

                            if($assignedSections>0){
                                $confirmMsg.="\r\nIt has $assignedSections section(s)";
                            }

                            $confirmMsg.="\r\n\r\nPlease consider to delete them or their relationships at first.";
                        }
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm' => $confirmMsg,
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>

</div>
