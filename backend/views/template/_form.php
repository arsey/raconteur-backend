<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Mediafile;
use backend\widgets\FileInput;
use backend\assets\HtmlSortableAsset;
use kartik\select2\Select2;
use common\models\Template;

HtmlSortableAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Template */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="template-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="form-group clearfix">
        <div class="col-sm-4">
            <?= $form->field($model, 'title')->textInput(['maxlength' => 150]) ?>
            <?= $form->field($model, 'type')->dropDownList(Template::$types) ?>
            <?= $form->field($model, 'comment')->textarea(['maxlength' => 255]) ?>
            <?= $form->field($model, 'status')->radioList(['disabled' => 'Disabled', 'active' => 'Active',], ['unselect' => 'active']) ?>
        </div>

        <div class="col-sm-8">
            <?= $form->field($model, 'html')->textarea(['rows' => 3]) ?>
            <?= $form->field($model, 'css')->textarea(['rows' => 3]) ?>

            <?= Select2::widget([
                'name' => 'scripts-select',
                'value' => '',
                'data' => $model->scripts,
                'options' => [
                    'placeholder' => 'Add a script...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => 'function(e) { raconteur.addScript(this,"Template[scriptIds][]");}',
                ]
            ]); ?>

            <ul class="scripts-sortable" data-target="template-scriptids">
                <?php
                if ($scriptsAssigned = $model->scriptTemplateAssignments) {
                    foreach ($scriptsAssigned as $sa) {
                        ?>
                        <li>
                            <?= $model->scripts[$sa->script_id] ?>
                            <input type="hidden" value="<?= $sa->script_id ?>" name="Template[scriptIds][]">
                            <span class="remove">x</span>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>

        </div>
    </div>

    <hr>

    <div class="form-group clearfix">

        <?= $form->field($model, 'preview_img')->hiddenInput() ?>

        <div class="col-sm-4">
            <?php
            echo FileInput::widget([
                'name' => 'template-preview',
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-info'],
                'options' => ['class' => 'hidden'],
                'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_IMG,
                'imageContainer' => '#template-preview-img',
                'template' => '<div class="input-group">{input}{button}</div>',
                'pasteData' => FileInput::DATA_URL,
                'callbackBeforeInsert' => 'function(e, data) {
                    $("input[name=\'Template[preview_img]\']").val(data.id);
                }',
            ]);
            ?>
        </div>
        <div class="col-sm-8" id="template-preview-img">
            <?php
            if ($model->previewImg) {
                echo Html::img($model->previewImg->defaultThumbUrl);
            }
            ?>
        </div>
    </div>

    <hr>

    <div class="form-group clearfix">
        <h5>Template images</h5>

        <div class="col-sm-4">
            <?= FileInput::widget([
                'name' => 'template-images',
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-info'],
                'options' => ['class' => 'hidden'],
                'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_IMG,
                'template' => '<div class="input-group">{input}{button}</div>',
                'pasteData' => FileInput::DATA_URL,
                'callbackBeforeInsert' => 'function(e, data) { raconteur.addImage(data,"Template[imageIds][]");}',
            ]); ?>
        </div>

        <ul class="col-sm-8" id="images-assignments">
            <?php
            if ($imagesAssigned = $model->templateImageAssignments) {
                foreach ($imagesAssigned as $ia) {
                    $file = $ia->file;
                    ?>
                    <li>
                        <?= $file->name; ?>
                        <input type="hidden" value="<?= $ia->file_id ?>" name="Template[imageIds][]">
                        <button type="button" class="close remove" aria-label="Remove"><span
                                aria-hidden="true">&times;</span></button>
                    </li>
                <?php
                }
            }
            ?>
        </ul>
    </div>


    <hr>

    <div class="form-group clearfix">
        <h5>Template fonts</h5>

        <div class="col-sm-4">
            <?php /* FileInput::widget([
                'name' => 'template-fonts',
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-info'],
                'options' => ['class' => 'hidden'],
                'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_FONT,
                'template' => '<div class="input-group">{input}{button}</div>',
                'pasteData' => FileInput::DATA_URL,
                'callbackBeforeInsert' => 'function(e, data) { raconteur.addFont(data,"Template[fontIds][]");}',
            ]); */ ?>
        </div>

        <ul class="col-sm-8" id="font-assignments">
            <?php
           /* if (false && $fontsAssigned = $model->templateFontAssignments) {
                foreach ($fontsAssigned as $fa) {
                    $file = $fa->file;
                    ?>
                    <li>
                        <?= Html::span($file->defaultThumbUrl); ?>
                        <input type="hidden" value="<?= $ia->file_id ?>" name="Template[fontIds][]">
                        <button type="button" class="close remove" aria-label="Remove">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </li>
                <?php
                }
            } */
            ?>
        </ul>
    </div>

    <hr>

    <?php if (!$model->isNewRecord) { ?>
        <div class="row updated">
            <div class="col-lg-12">
                Users : <?= $model->userTemplateAssignmentNumber; ?>&nbsp;
                <?= Html::a('Manage users', ['/user/admin', 'UserSearch[ids]' => $model->usersIdsAssignedToTemplate], ['class' => 'btn btn-warning btn-xs', 'target' => '_blank']) ?>
            </div>
        </div>

        <div class="row updated">
            <div class="col-lg-12">
                Sections : <?= $model->sectionsNumber ?>&nbsp;
                <?= Html::a('Manage sections', ['/section/index', 'SectionSearch[template_id]' => $model->id], ['class' => 'btn btn-warning btn-xs', 'target' => '_blank']) ?>
            </div>
        </div>

        <?= Html::a('Assign template to a new user', ['/admin/create', 'bespokenTemplate' => $model->id], ['class' => 'btn btn-success btn-xs', 'target' => '_blank']) ?>
    <?php } ?>

    <br/>
    <br/>
    <hr>

    <div class="form-group clearfix">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

        <?= Html::a('Cansel', ['index'], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>