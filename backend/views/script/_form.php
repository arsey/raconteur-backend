<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Script;
use backend\widgets\FileInput;
use backend\models\Mediafile;

/* @var $this yii\web\View */
/* @var $model common\models\Script */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="script-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea() ?>

    <?= $form->field($model, 'file_id')->hiddenInput() ?>

    <div class="form-group">
        <?php
        $file = $model->file;
        $scriptFilePath = '';
        if ($file) {
            $scriptFilePath = $file->url;
        }
        echo FileInput::widget([
            'name' => 'script-file',
            'buttonTag' => 'button',
            'buttonName' => 'Browse',
            'buttonOptions' => ['class' => 'btn btn-info'],
            'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_SCRIPT,
            // Widget template
            'template' => '
                <div class="input-group">
                    <input type="text" id="w1" class="form-control" name="script-file" value="'.$scriptFilePath.'">
                    <span class="input-group-btn">{button}</span>
                </div>
',
            // Default to FileInput::DATA_URL. This data will be inserted in input field
            'pasteData' => FileInput::DATA_URL,
            // JavaScript function, which will be called before insert file data to input.
            // Argument data contains file data.
            'callbackBeforeInsert' => 'function(e, data) {
            var $fileId=$("input[name=\'Script[file_id]\']");
            $fileId.val(data.id);
        }',
        ]);
        ?>
    </div>

    <?= $form->field($model, 'type')->dropDownList(Script::$types) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>