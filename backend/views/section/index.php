<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sections';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'index'];
?>
<div class="section-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Section', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            [
                'attribute' => 'template',
                'value' => 'template.title'
            ],
            'type',
            'title',
            'tags',
            [
                'format' => 'image',
                'value' => function ($data) {
                    if($image=$data->previewImageModel) {
                        return $image->defaultThumbUrl;
                    }
                },
                'header' => 'Image Preview',
                'headerOptions' => ['style' => 'width: 128px;'],
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>