<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\assets\HtmlSortableAsset;
use backend\models\Mediafile;
use backend\widgets\FileInput;

HtmlSortableAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="form-group clearfix">
        <div class="col-sm-4">

            <div class="form-group">
                <?= $form->field($model, 'templateIds')->widget(Select2::classname(), [
                    'data' => $model->templates,
                    'options' => [
                        'placeholder' => 'Template',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]); ?>
            </div>

            <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'type')->dropDownList(array_merge(['' => 'Choose type'], $model::$types)) ?>
            <?= $form->field($model, 'tags')->textInput(['maxlength' => 255]) ?>
        </div>
    </div>

    <!--HTML-->
    <div class="form-group clearfix">
        <div class="col-sm-12">
            <?= $form->field($model, 'html')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <!--CSS-->
    <div class="form-group clearfix">
        <div class="col-sm-12">
            <?= $form->field($model, 'css')->textarea(['rows' => 3]) ?>
        </div>
    </div>

    <hr>

    <!--Section Scripts-->
    <div class="form-group clearfix">
        <div class="col-sm-5">

            <?= Select2::widget([
                'name' => 'scripts-select',
                'value' => '',
                'data' => $model->scripts,
                'options' => [
                    'placeholder' => 'Add a script...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => 'function(e) { raconteur.addScript(this,"Section[scriptIds][]");}',
                ]
            ]); ?>

            <ul class="scripts-sortable" data-target="template-scriptids">
                <?php
                if ($scriptsAssigned = $model->scriptSectionAssignments) {
                    foreach ($scriptsAssigned as $sa) {
                        ?>
                        <li>
                            <?= $model->scripts[$sa->script_id] ?>
                            <input type="hidden" value="<?= $sa->script_id ?>" name="Section[scriptIds][]">
                            <span class="remove">x</span>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>

    <hr>

    <!--Status-->
    <div class="form-group clearfix">
        <div class="col-sm-12">
            <?= $form->field($model, 'status')->radioList(['active' => 'Active', 'disabled' => 'Disabled'], ['prompt' => '']) ?>
        </div>
    </div>

    <hr>

    <!--Preview Image-->
    <div class="form-group clearfix">
        <div class="col-sm-12">
            <?= $form->field($model, 'preview_img')->hiddenInput() ?>
            <div class="col-sm-4 nopadding">
                <?php
                echo FileInput::widget([
                    'name' => 'section-preview',
                    'buttonTag' => 'button',
                    'buttonName' => 'Browse',
                    'buttonOptions' => ['class' => 'btn btn-info btn-sm'],
                    'options' => ['class' => 'hidden'],
                    'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_IMG,
                    'imageContainer' => '#section-preview-img',
                    'template' => '<div class="input-group">{input}{button}</div>',
                    'pasteData' => FileInput::DATA_URL,
                    'callbackBeforeInsert' => 'function(e, data) {
                    $("input[name=\'Section[preview_img]\']").val(data.id);
                }',
                ]);
                ?>
            </div>
            <div class="col-sm-6 col-sm-offset-2" id="section-preview-img">
                <?php
                if ($model->previewImg) {
                    echo Html::img($model->previewImg->defaultThumbUrl);
                }
                ?>
            </div>
        </div>
    </div>

    <hr>

    <div class="form-group clearfix">
        <div class="col-sm-4">
            <h4>Images</h4>

            <?= FileInput::widget([
                'name' => 'section-images',
                'buttonTag' => 'button',
                'buttonName' => 'Browse',
                'buttonOptions' => ['class' => 'btn btn-info btn-sm'],
                'options' => ['class' => 'hidden'],
                'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_ADMIN_IMG,
                'template' => '<div class="input-group">{input}{button}</div>',
                'pasteData' => FileInput::DATA_URL,
                'callbackBeforeInsert' => 'function(e, data) { raconteur.addImage(data,"Section[imageIds][]");}',
            ]); ?>
        </div>

        <ul class="col-sm-6 col-sm-offset-2" id="images-assignments">
            <?php
            if ($imagesAssigned = $model->sectionImageAssignments) {
                foreach ($imagesAssigned as $ia) {
                    $file = $ia->file;
                    ?>
                    <li>
                        <?= Html::img($file->defaultThumbUrl); ?>
                        <input type="hidden" value="<?= $ia->file_id ?>" name="Section[imageIds][]">
                        <button type="button" class="close remove" aria-label="Remove"><span
                                aria-hidden="true">&times;</span></button>
                    </li>
                <?php
                }
            }
            ?>
        </ul>
    </div>

    <hr>

    <div class="form-group">
        <div class="col-sm-12">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cansel', ['index'], ['class' => 'btn btn-warning']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>