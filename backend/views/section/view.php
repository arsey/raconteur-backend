<?php
use yii\helpers\Html;
use backend\assets\HighlightjsAsset;

HighlightjsAsset::register($this);
/* @var $this yii\web\View */
/* @var $model common\models\Section */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Sections', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-view">

    <div class="container">

        <!--Section Title-->
        <h2>Section: <?= Html::encode($this->title) ?></h2>

        <!--Image Preview-->
        <div class="jumbotron">
            <?php
            if ($model->preview_img) {
                echo Html::img($model->previewImageModel->thumbs['medium']);
            }
            ?>
        </div>

        <!--Section Type-->
        <div class="row mar-tb-10">
            <div class="col-lg-4">Type: <?= $model->type; ?></div>
        </div>

        <!--HTML-->
        <?php if ($model->html) { ?>
            <section class="row mar-tb-10">
                <h4>Section Html</h4>
                <pre><code class="xml"><?= htmlspecialchars($model->html); ?></code></pre>
            </section>
        <?php } ?>

        <!--CSS-->
        <?php if ($model->css) { ?>
            <section class="row mar-tb-10">
                <h4>Section CSS</h4>
                <pre><code class="css"><?= htmlspecialchars($model->css); ?></code></pre>
            </section>
        <?php } ?>

        <!--Last updated-->
        <div class="row date-modified mar-tb-10">
            <div class="col-lg-12">
                Last updated : <?= date("F j, Y", $model->modified); ?>
            </div>
        </div>

        <div class="row mar-tb-10">
            <div class="col-lg-12">
                <h4>Scripts</h4>
                <ul>
                    <?php
                    if ($scriptsAssigned = $model->scriptSectionAssignments) {
                        foreach ($scriptsAssigned as $sa) {
                            ?>
                            <li>
                                <?= $model->scripts[$sa->script_id]; ?>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>

    </div>

    <!--Buttons-->
    <div class="mar-tb-10">
        <div class="col-xs-12">
            <?= Html::a('Back to sections', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Edit section', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

</div>