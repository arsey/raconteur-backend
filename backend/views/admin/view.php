<?php

use yii\helpers\Html;
use \common\models\SubscriptionUserAssignment;
use api\modules\v1\models\Mediafile;
use backend\widgets\FileInput;

/**
 * @var yii\web\View $this
 * @var amnah\yii2\user\models\User $user
 * @var amnah\yii2\user\models\Profile $profile
 * @var yii\widgets\ActiveForm $form
 */

$this->title = $user->profile['first_name'] . ' ' . $user->profile['last_name'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <?php
                if (is_numeric($user->profile->photo)) {
                    $photo = Mediafile::findOne($user->profile->photo);
                    if ($photo) {
                        echo  Html::img($photo->defaultThumbUrl);
                    }
                }
                ?>
                <div style="color:#0088CC"><?= Html::encode($this->title) ?><?= $user->profile['company'] ? ', ' . $user->profile['company'] : '' ?></div>


            </div>
            <?php
            $subscription = $user->getSubscriptionUserAssignment([SubscriptionUserAssignment::STATUS_ACTIVE])->one();
            $user->activeSubscription = $subscription;
            if ($subscription) {
                ?>

                <div class="col-md-4">
                    <span>Subscription: <?= $user->activeSubscription->subscription->name; ?></span>

                </div>

                <div class="col-md-4">
                    <?= "Subscription Expires on " . date('Y-m-d H:i', $subscription->end_date) ?>
                </div>
            <?php } ?>
        </div>

        <div class="row user-personal-data">
            <div class="col-lg-4">
                <h4>Personal data</h4>

                <div class="tbl">
                    <div class="tbl-tow">
                        <div class="tbl-cell">Company:</div>
                        <div class="tbl-cell"><?= $user->profile['company']; ?></div>
                    </div>
                    <div class="tbl-tow">
                        <div class="tbl-cell">Position:</div>
                        <div class="tbl-cell"><?= $user->profile['position']; ?></div>
                    </div>
                    <div class="tbl-tow">
                        <div class="tbl-cell">Email:</div>
                        <div class="tbl-cell"><?= $user->email; ?></div>
                    </div>
                    <div class="tbl-tow">
                        <div class="tbl-cell">Phone:</div>
                        <div class="tbl-cell"><?= $user->profile['phone']; ?></div>
                    </div>
                </div>
            </div>

            <?php if ($requests = $user->userRequests) { ?>
                <div class="col-lg-8">
                    <h4>Requests from user</h4>

                    <ul>
                        <?php foreach ($requests as $request) { ?>
                            <li><?= Html::a(date('Y-m-d H:i', $request->created) . ' ' . @\common\models\UserRequest::$types[$request->type] . " ({$request->status})", ['/request/view', 'id' => $request->id]) ?></li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>

        </div>


        <section class="user-activity-data">
            <h4>Activity</h4>
            <ul class="model-templates" style="width: 200px">
                <?php
                if ($templatesAssigned = $user->userTemplateAssignments) {
                    foreach ($templatesAssigned as $ta) {
                        if (isset($user->templates[$ta->template_id])) {
                            ?>
                            <li>
                                <?= Html::a($user->templates[$ta->template_id], ['/template/view', 'id' => $ta->template_id]) ?>
                            </li>
                        <?php
                        }
                    }
                }
                ?>
            </ul>
        </section>

        <div class="row">
            <div class="col-lg-10">
                <?= FileInput::widget([
                    'name' => 'section-images',
                    'buttonTag' => 'button',
                    'buttonName' => 'User Files',
                    'buttonOptions' => ['class' => 'btn btn-info btn-sm'],
                    'options' => ['class' => 'hidden'],
                    'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_FILE_FROM_THIRD_APP,
                    'ownerId' => $user->id,
                    'template' => '<div class="input-group">{input}{button}</div>',
                    'pasteData' => FileInput::DATA_URL,
                ]); ?>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-lg-10">
                Last login : <?= $user->login_time; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-10">
                Sessions : <?= $user->profile['sessions']; ?>
            </div>
        </div>

    </div>

    <!--Buttons-->
    <div class="mar-tb-10">
        <div class="col-xs-12">
            <?= Html::a('Back to users', ['/user/admin',], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Edit user', ['user/admin/update', 'id' => $user->id], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

</div>