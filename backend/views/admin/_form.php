<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use api\modules\v1\models\Mediafile;
use backend\widgets\FileInput;

$role = Yii::$app->getModule("user")->model("Role");

/**
 * @var yii\web\View $this
 * @var amnah\yii2\user\models\User $user
 * @var amnah\yii2\user\models\Profile $profile
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary([$user, $profile]) ?>

    <div class="form-group clearfix">
        <div class="col-sm-4">
            <?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($user, 'newPassword')->passwordInput() ?>
            <?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
            <?= $form->field($profile, 'first_name'); ?>
            <?= $form->field($profile, 'last_name'); ?>
            <?= $form->field($profile, 'company'); ?>
            <?= $form->field($profile, 'position'); ?>

            <?= FileInput::widget([
                'name' => 'section-images',
                'buttonTag' => 'button',
                'buttonName' => 'User Files',
                'buttonOptions' => ['class' => 'btn btn-info btn-sm'],
                'options' => ['class' => 'hidden'],
                'ownerAttribute' => Mediafile::OWNER_ATTRIBUTE_FILE_FROM_THIRD_APP,
                'ownerId' => $user->id,
                'template' => '<div class="input-group">{input}{button}</div>',
                'pasteData' => FileInput::DATA_URL,
            ]); ?>
        </div>
        <div class="col-sm-4">

            <!--Subscription-->
            <?= $form->field($user, 'subscriptionId')->widget(Select2::classname(), [
                'data' => $user->subscriptions,
                'options' => [
                    'placeholder' => 'Choose a subscription...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

            <!--Assigned Templates-->
            <div class="form-group">
                <label class="control-label" for="user-subscriptionid">Template</label>
                <?= Select2::widget([
                    'name' => 'templates-select',
                    'value' => '',
                    'data' => $user->templates,
                    'options' => [
                        'placeholder' => 'Add a template...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                    'pluginEvents' => [
                        "change" => 'function(e) { raconteur.addTemplate(this,"User[templateIds][]");}',
                    ]
                ]); ?>
                <ul class="model-templates">
                    <?php
                    if ($templatesAssigned = $user->userTemplateAssignments) {
                        foreach ($templatesAssigned as $ta) {
                            if(isset($user->templates[$ta->template_id])){
                                ?>
                                <li>
                                    <?= $user->templates[$ta->template_id] ?>
                                    <input type="hidden" value="<?= $ta->template_id ?>" name="User[templateIds][]">
                                    <span class="remove">x</span>
                                </li>
                            <?php
                            }
                        }
                    }
                    if ($user->bespokenTemplate) {
                        ?>
                        <li>
                            <?= $user->templates[$user->bespokenTemplate] ?>
                            <input type="hidden" value="<?= $user->bespokenTemplate ?>" name="User[templateIds][]">
                            <span class="remove">x</span>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>

            <?= $form->field($user, 'role_id')->dropDownList($role::dropdown(), ['prompt' => 'Choose...']); ?>

            <?= $form->field($user, 'status')->dropDownList($user::statusDropdown()); ?>

            <?= $form->field($profile, 'phone'); ?>

        </div>
    </div>

    <hr>

    <div class="form-group">
        <div class="col-sm-12">
            <?= Html::submitButton($user->isNewRecord ? 'Create' : 'Update', ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cansel', ['index'], ['class' => 'btn btn-warning']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>