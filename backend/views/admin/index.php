<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \common\models\SubscriptionUserAssignment;

$user = Yii::$app->getModule("user")->model("User");
$role = Yii::$app->getModule("user")->model("Role");

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var amnah\yii2\user\models\search\UserSearch $searchModel
 * @var amnah\yii2\user\models\User $user
 * @var amnah\yii2\user\models\Role $role
 */

$this->title = Yii::t('user', 'Users');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'index'];
?>
<div class="user-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('user', 'Create {modelClass}', [
            'modelClass' => 'User',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            'username',
            'email:email',
            'profile.company',
            [
                'attribute' => 'status',
                'label' => Yii::t('user', 'Status'),
                'filter' => $user::statusDropdown(),
                'value' => function ($model, $index, $dataColumn) use ($user) {
                    $statusDropdown = $user::statusDropdown();
                    return $statusDropdown[$model->status];
                },
            ],
            [
                'header' => 'Status',
                'format' => 'html',
                'value' => function ($data) {
                    $subscription = $data->getSubscriptionUserAssignment([SubscriptionUserAssignment::STATUS_ACTIVE])->one();
                    if ($subscription) {
                        $data->activeSubscription = $subscription;
                        return "expires on<br>" . date('Y-m-d H:i', $subscription->end_date);
                    }
                }
            ],
            'login_time',
            [
                'header' => '#saved',
                'value' => function ($data) {
                    if ($data->activeSubscription) {
                        return $data->activeSubscription->pages_saved . "/" . $data->activeSubscription->subscription->pages_allowed;
                    }
                }
            ],
            [
                'header' => '#downloads',
                'value' => function ($data) {
                    if ($data->activeSubscription) {
                        return $data->activeSubscription->downloads . "/" . $data->activeSubscription->subscription->downloads_allowed;
                    }
                }
            ],
            [
                'header' => '#published',
                'value' => function ($data) {
                    if ($data->activeSubscription) {
                        return $data->activeSubscription->pages_published . "/" . $data->activeSubscription->subscription->publishes_allowed;
                    }
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
