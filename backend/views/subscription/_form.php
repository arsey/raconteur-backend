<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Subscription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subscription-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model) ?>

    <div class="row-fluid clearfix">
        <div class="col-sm-4 nopadding-left">

            <!--Name-->
            <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>

            <!--Templates-->
            <?= Select2::widget([
                'name' => 'templates-select',
                'value' => '',
                'data' => $model->templates,
                'options' => [
                    'placeholder' => 'Add a template...',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'pluginEvents' => [
                    "change" => 'function(e) { raconteur.addTemplate(this,"Subscription[templateIds][]");}',
                ]
            ]); ?>
            <ul class="model-templates">
                <?php
                if ($templatesAssigned = $model->subscriptionTemplateAssignments) {
                    foreach ($templatesAssigned as $ta) {
                        ?>
                        <li>
                            <?= $model->templates[$ta->template_id] ?>
                            <input type="hidden" value="<?= $ta->template_id ?>" name="Subscription[templateIds][]">
                            <span class="remove">x</span>
                        </li>
                    <?php
                    }
                }
                ?>
            </ul>

            <!--Price-->
            <?= $form->field($model, 'price')->input('number', ['maxlength' => 10]) ?>

            <!--Status-->
            <?= $form->field($model, 'status')->radioList(['active' => 'Active', 'disabled' => 'Disabled',], ['prompt' => '']) ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'period')->dropDownList(array_merge(['' => 'Select...'], $model::$periods)) ?>

            <?= $form->field($model, 'pages_allowed')->input('number') ?>

            <?= $form->field($model, 'downloads_allowed')->input('number') ?>

            <?= $form->field($model, 'publishes_allowed')->input('number') ?>
        </div>
    </div>

    <div class="row-fluid clearfix">
        <div class="col-sm-8 nopadding-left">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <hr>

    <div class="row-fluid">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cansel', ['index'], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>