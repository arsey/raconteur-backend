<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Subscription;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscriptions';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => 'index'];
?>
<div class="subscription-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Subscription', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 80px;'],
            ],
            'name',
            [
                'attribute' => 'period',
                'value' => function ($data) {
                    return Subscription::$periods[$data->period];
                }
            ],
            [
                'header' => 'Users',
                'format' => 'html',
                'value' => function ($data) {
                    $users = $data->users;
                    $usersIds = [];
                    if ($users) {
                        foreach ($users as $user) {
                            $usersIds[] = $user->id;
                        }
                    }
                    return Html::a(count($usersIds), ['/user/admin', 'UserSearch[ids]' => $usersIds]);
                }
            ],
            [
                'attribute' => 'templatesAssigned',
                'value' => function ($data) {

                    return count($data->templatesAssigned);
                }
            ],
            'pages_allowed',
            'downloads_allowed',
            'publishes_allowed',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>