<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Subscription */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-view">

    <div class="container">

        <h2><?= Html::encode($model->name) ?> (<?= $model->price ? '$' . $model->price : 'free' ?>)</h2>

        <div class="row">
            <div class="col-lg-5">
                <div class="row">
                    <div class="col-lg-5">Period of
                        activity: <?= \common\models\Subscription::$periods[$model->period]; ?></div>
                </div>
            </div>

            <?php $notSet = '<span class="not-set">(not set)</span>'; ?>
            <section class="subscription-data col-lg-5">
                <div class="row">
                    <div class="col-lg-12">
                        Saved pages allowed:&nbsp;<?= $model->pages_allowed ? $model->pages_allowed : $notSet; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">Downloads
                        allowed:&nbsp;<?= $model->downloads_allowed ? $model->downloads_allowed : $notSet; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">Published pages
                        allowed:&nbsp;<?= $model->publishes_allowed ? $model->publishes_allowed : $notSet; ?>
                    </div>
                </div>
            </section>
        </div>

        <div class="row mar-tb-10">
            <div class="col-lg-12 nopadding-left">
                <h4>Templates Included:</h4>
                <ul class="subscription-included-templates">
                    <?php
                    if ($templatesAssigned = $model->templatesAssigned) {
                        foreach ($templatesAssigned as $ta) {
                            ?>
                            <li>
                                <?php
                                $img = '';
                                if ($ta->preview_img) {
                                    $img = Html::img($ta->previewImageModel->thumbs['small']);
                                }
                                ?>
                                <?= Html::a($img . $ta->title, ['/template/view', 'id' => $ta->id]) ?>
                            </li>
                        <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>

        <div class="row mar-tb-10">
            <div class="col-lg-10 nopadding-left">
                Last updated : <?= date("F j, Y H:i", $model->modified); ?>
            </div>
        </div>

    </div>

    <!--Buttons-->
    <div class="mar-tb-10">
        <div class="col-xs-12">
            <?= Html::a('Back to subscriptions', ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Edit subscription', ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

</div>