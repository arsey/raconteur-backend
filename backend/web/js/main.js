$(function () {

    var $body = $('body');

    $body
        .on('click', '.model-templates .remove', function () {
            $(this).parents('li').remove();
        });
    //////////////////////////////////////////////////////////////////////
    //--------------------TEMPLATE PAGE
    //////////////////////////////////////////////////////////////////////

    var $templateScriptsSortable = $(".scripts-sortable");
    if ($templateScriptsSortable.length) {
        $templateScriptsSortable.sortable();
    }

    $body
        .on('click', '.scripts-sortable .remove', function () {
            $(this).parents('li').remove();
        })
        .on('click', '#images-assignments li .remove', function () {
            $(this).parents('li').remove();
        })
        .on('submit', '.template-form form', function (e) {
            //log($(this).serializeArray())
            //e.preventDefault();
        });
    //////////////////////////////////////////////////////////////////////

    //////////////////////////////////////////////////////////////////////
    //--------------------SUBSCRIPTION PAGE
    //////////////////////////////////////////////////////////////////////
    $body
        .on('click', '.subscription-templates .remove', function () {
            $(this).parents('li').remove();
        });
    //////////////////////////////////////////////////////////////////////
    if (window.hljs)
        hljs.initHighlightingOnLoad();
});


(function () {
    window.raconteur = {};

    raconteur.addImage = function (data, inputNameAttr) {
        var $input = $('<input type="hidden">').val(data.id).attr('name', inputNameAttr);
        var $fontWrapper = $('<li>');
        var $removeIcon = $('<span>').addClass('remove').text('x');
        var $text = $('<span>').text(data.url);
        $fontWrapper.append($text).append($removeIcon).append($input);
        $('#font-assignments').append($fontWrapper);
    };

    raconteur.addImage = function (data, inputNameAttr) {
        var $input = $('<input type="hidden">').val(data.id).attr('name', inputNameAttr);
        var $imgWrapper = $('<li>');
        var $removeIcon = $('<span>').addClass('remove').text('x');
        var $img = $('<img>').attr('src', data.url);
        $imgWrapper.append($img).append($removeIcon).append($input);
        $('#images-assignments').append($imgWrapper);
    };

    raconteur.addScript = function (el, inputNameAttr) {
        var $el = $(el);
        var data = $el.select2("data");

        //add to sortable
        var $li = $('<li>').text(data.text);
        var $removeIcon = $('<span>').addClass('remove').text('x');
        var $input = $('<input type="hidden">').val(data.id).attr('name', inputNameAttr);
        $(".scripts-sortable").append($li.append($input).append($removeIcon)).sortable();

        //clear select2
        $el.select2("val", "");
    };

    raconteur.addTemplate = function (el, inputNameAttr) {
        var $el = $(el);
        var data = $el.select2("data");
        var $holder = $(".model-templates");

        if ($holder.find('input[value="' + data.id + '"]').length) {
            alert('This template already added!');
            return;
        }

        var $li = $('<li>').text(data.text);
        var $removeIcon = $('<span>').addClass('remove').text('x');
        var $input = $('<input type="hidden">').val(data.id).attr('name', inputNameAttr);

        $holder.append($li.append($input).append($removeIcon));

        //clear select2
        $el.select2("val", "");
    };
})();


window.log = function () {
    log.history = log.history || [];   // store logs to an array for reference
    log.history.push(arguments);
    if (this.console) {
        console.log(Array.prototype.slice.call(arguments));
    }
};