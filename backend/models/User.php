<?php

namespace backend\models;

use common\models\Subscription;
use common\models\SubscriptionUserAssignment;
use common\models\UserRequest;
use common\models\UserTemplateAssignment;

class User extends \amnah\yii2\user\models\User
{
    public $subscriptionId = [];
    public $templateIds = [];
    public $bespokenTemplate;

    public $templates = [];
    public $subscriptions = [];

    public $activeSubscription;

    public function rules()
    {
        $parentRules = parent::rules();
        $parentRules[] = [['subscriptionId', 'templateIds'], 'safe'];
        return $parentRules;
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['subscriptionId'] = 'Subscription';
        return $labels;
    }

    public function afterSave($insert, $changedAttributes)
    {
        /** assign subscription to the user if subscriptionId is present */
        if (!empty($this->subscriptionId) && $subscription = Subscription::find()->where(['id' => $this->subscriptionId])->one())
            SubscriptionUserAssignment::assignToUser($this->id, $subscription);
        else
            SubscriptionUserAssignment::assignToUser($this->id, null);

        /** assign templates to the user */
        if (!empty($this->templateIds))
            UserTemplateAssignment::assignToUser($this->id, $this->templateIds);

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTemplateAssignments()
    {
        return $this->hasMany(UserTemplateAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionUserAssignment(array $status = [])
    {
        $query = $this->hasMany(SubscriptionUserAssignment::className(), ['user_id' => 'id']);

        if (!empty($status)) {
            $query->where(['status' => $status]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserRequests()
    {
        return $this->hasMany(UserRequest::className(), ['user_id' => 'id']);
    }
}
