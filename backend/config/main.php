<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'backend-raconteur',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'user' => [
            'class' => 'amnah\yii2\user\Module',
            'modelClasses' => [
                'Profile' => 'common\models\Profile',
                'UserSearch' => 'common\models\search\UserSearch',
                'User' => 'backend\models\User',
            ],
        ],
        'filemanager' => [
            'class' => 'pendalf89\filemanager\Module',
            // Upload routes
            'routes' => [
                // Base absolute path to web directory
                'baseUrl' => '',
                // Base web directory url
                'basePath' => '@mainRootWeb',
                // Path for uploaded files in web directory
                'uploadPath' => 'uploads',
            ],
            // Thumbnails info
            'thumbs' => [
                'small' => [
                    'name' => 'Small',
                    'size' => [100, 100],
                ],
                'medium' => [
                    'name' => 'Medium',
                    'size' => [300, 200],
                ],
                'large' => [
                    'name' => 'Large',
                    'size' => [500, 400],
                ],
            ],
        ],
    ],
    'components' => [
        'assetManager' => [
            'linkAssets' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'file/info' => 'filemanager/file/info',
                'user/admin/update' => 'admin/update',
                'user/admin' => 'admin/index',
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '4DT96bnc5YqSnHDNP0xTachk8eiJtMal',

        ],
        'user' => [
            'class' => 'amnah\yii2\user\components\User',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/amnah/yii2-user/views' => '@backend/views/user',
                    '@vendor/pendalf89/yii2-filemanager/views' => '@backend/views/filemanager',
                ]
            ]
        ]
    ],
    'params' => $params,
];