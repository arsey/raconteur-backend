<?php

namespace backend\controllers;

use common\models\Subscription;
use common\models\SubscriptionUserAssignment;
use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use common\models\Template;


class AdminController extends \amnah\yii2\user\controllers\AdminController
{

    /**
     * Display a single User model
     *
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if ($model)
            $model->templates = ArrayHelper::map(Template::find()->where(['status' => Template::STATUS_ACTIVE])->all(), 'id', 'title');

        return $this->render('view', [
            'user' => $model,
        ]);
    }

    /**
     * Create a new User model. If creation is successful, the browser will
     * be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        /** @var \amnah\yii2\user\models\User $user */
        /** @var \amnah\yii2\user\models\Profile $profile */

        $user = Yii::$app->getModule("user")->model("User");
        $user->setScenario("admin");
        $profile = Yii::$app->getModule("user")->model("Profile");

        $post = Yii::$app->request->post();
        if ($user->load($post) && $user->validate() && $profile->load($post) && $profile->validate()) {
            $user->save(false);
            $profile->setUser($user->id)->save(false);
            return $this->redirect(['view', 'id' => $user->id]);
        }

        /** templates for select2 */
        $user->templates = ArrayHelper::map(Template::find()->where(['status' => Template::STATUS_ACTIVE])->all(), 'id', 'title');

        /** subscriptions for select2 */
        $user->subscriptions = ArrayHelper::map(Subscription::find()->where(['status' => Template::STATUS_ACTIVE])->all(), 'id', 'name');

        if ($currentUserSubscription = SubscriptionUserAssignment::getUserActiveSubscription($user->id)) {
            $user->subscriptionId = $currentUserSubscription->subscription_id;
        }

        if ($bespokenTemplate = Yii::$app->request->getQueryParam('bespokenTemplate', null)) {
            $user->bespokenTemplate = $bespokenTemplate;
        }

        // render
        return $this->render('create', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Update an existing User model. If update is successful, the browser
     * will be redirected to the 'view' page.
     *
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // set up user and profile
        $user = $this->findModel($id);
        $user->setScenario("admin");
        $profile = $user->profile;

        // load post data and validate
        $post = Yii::$app->request->post();
        if ($user->load($post) && $user->validate() && $profile->load($post) && $profile->validate()) {
            $user->save(false);
            $profile->setUser($user->id)->save(false);
            return $this->redirect(['view', 'id' => $user->id]);
        }

        /** templates for select2 */
        $user->templates = ArrayHelper::map(Template::find()->where(['status' => Template::STATUS_ACTIVE])->all(), 'id', 'title');

        /** subscriptions for select2 */
        $user->subscriptions = ArrayHelper::map(Subscription::find()->where(['status' => Template::STATUS_ACTIVE])->all(), 'id', 'name');

        if ($currentUserSubscription = SubscriptionUserAssignment::getUserActiveSubscription($user->id)) {
            $user->subscriptionId = $currentUserSubscription->subscription_id;
        }

        // render
        return $this->render('update', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * List all User models
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var \amnah\yii2\user\models\search\UserSearch $searchModel */
        $searchModel = Yii::$app->getModule("user")->model("UserSearch");
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    protected function findModel($id)
    {
        /** @var \amnah\yii2\user\models\User $user */
        $user = new \backend\models\User;
        if (($user = $user::findOne($id)) !== null) {
            return $user;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}