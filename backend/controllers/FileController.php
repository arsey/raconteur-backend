<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use backend\models\Mediafile;
use pendalf89\filemanager\assets\FilemanagerAsset;
use yii\helpers\Url;
use yii\filters\AccessControl;

class FileController extends Controller
{

    public $enableCsrfValidation = false;

    private $fileManager = null;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function beforeAction($action)
    {
        $this->fileManager = Yii::$app->getModule('filemanager');

        return parent::beforeAction($action);
    }

    public function actionFilemanager($owner_attribute = null, $owner_id = null)
    {
        $this->layout = '@app/views/layouts/filemanager';
        $model = new Mediafile();
        $dataProvider = $model->search($owner_attribute, $owner_id);
        $dataProvider->pagination->defaultPageSize = 10;

        return $this->render('filemanager', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Provides upload file
     * @return mixed
     */
    public function actionUpload($owner_attribute = null, $owner_id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Mediafile();


        $routes = $this->fileManager->routes;

        if ($model->saveUploadedFile($routes)) {
            $userId = Yii::$app->user->id;
            $userEmail = Yii::$app->user->identity->email;

            if ($owner_id) {
                $user = new \backend\models\User;
                if (($user = $user::findOne($owner_id)) !== null) {
                    $userId = $user->id;
                    $userEmail = $user->email;
                }
            }
            $model->addOwner($userId, $userEmail, $owner_attribute);
        }

        $bundle = FilemanagerAsset::register($this->view);

        if ($model->isImage()) {
            $model->createThumbs($routes, $this->fileManager->thumbs);
        }

        $response['files'][] = [
            'url' => $model->url,
            'thumbnailUrl' => $model->getDefaultThumbUrl($bundle->baseUrl),
            'name' => $model->filename,
            'type' => $model->type,
            'size' => $model->file->size,
            'deleteUrl' => Url::to(['file/delete', 'id' => $model->id]),
            'deleteType' => 'POST',
        ];

        return $response;
    }

    public function actionUploadmanager()
    {
        $this->layout = '@vendor/pendalf89/yii2-filemanager/views/layouts/main';
        return $this->render('uploadmanager', ['model' => new Mediafile()]);
    }


    /**
     * Delete model with files
     * @param $id
     * @return array
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $routes = $this->fileManager->routes;

        $model = Mediafile::findOne($id);

        if ($model->isImage()) {
        }

        $model->deleteFile($routes);
        $model->delete();

        return ['success' => 'true'];
    }

}
