<?php

namespace backend\assets;

use yii\web\AssetBundle;

class HighlightjsAsset extends AssetBundle
{
    public $sourcePath = "@bower/highlightjs";
    public $js = ["highlight.pack.js"];
    public $css = ['styles/monokai.css'];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
    ];
}