<?php

namespace backend\assets;

use yii\web\AssetBundle;

class HtmlSortableAsset extends AssetBundle
{
    public $sourcePath = "@bower/html.sortable/dist";
    public $js = ["html.sortable.min.js"];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}