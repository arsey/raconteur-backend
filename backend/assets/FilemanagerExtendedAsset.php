<?php

namespace backend\assets;

use yii\web\AssetBundle;

class FilemanagerExtendedAsset extends AssetBundle
{
    public $sourcePath = '@app/web/css';
    public $css = [
        'filemanager-extended.css',
    ];

}