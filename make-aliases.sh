#! /bin/sh

CUR_DIR=$(pwd)

ln -s "$CUR_DIR/web/uploads" "$CUR_DIR/backend/web/uploads"
ln -s "$CUR_DIR/web/uploads" "$CUR_DIR/api/web/uploads"

ln -s "$CUR_DIR/web/pub" "$CUR_DIR/backend/web/pub"
ln -s "$CUR_DIR/web/pub" "$CUR_DIR/api/web/pub"
