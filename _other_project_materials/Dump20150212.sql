-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: localhost    Database: raconteur
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `file_storage_item`
--

DROP TABLE IF EXISTS `file_storage_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_storage_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `repository` varchar(32) NOT NULL,
  `category` varchar(128) DEFAULT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `path` varchar(2048) NOT NULL,
  `mimeType` varchar(128) NOT NULL,
  `upload_ip` varchar(15) DEFAULT NULL,
  `size` int(11) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_storage_item`
--

LOCK TABLES `file_storage_item` WRITE;
/*!40000 ALTER TABLE `file_storage_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_storage_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filemanager_mediafile`
--

DROP TABLE IF EXISTS `filemanager_mediafile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filemanager_mediafile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `alt` text,
  `size` varchar(255) NOT NULL,
  `description` text,
  `thumbs` text,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filemanager_mediafile`
--

LOCK TABLES `filemanager_mediafile` WRITE;
/*!40000 ALTER TABLE `filemanager_mediafile` DISABLE KEYS */;
/*!40000 ALTER TABLE `filemanager_mediafile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filemanager_owners`
--

DROP TABLE IF EXISTS `filemanager_owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `filemanager_owners` (
  `mediafile_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `owner` varchar(255) NOT NULL,
  `owner_attribute` varchar(255) NOT NULL,
  PRIMARY KEY (`mediafile_id`,`owner_id`,`owner`,`owner_attribute`),
  CONSTRAINT `filemanager_owners_ref_mediafile` FOREIGN KEY (`mediafile_id`) REFERENCES `filemanager_mediafile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filemanager_owners`
--

LOCK TABLES `filemanager_owners` WRITE;
/*!40000 ALTER TABLE `filemanager_owners` DISABLE KEYS */;
/*!40000 ALTER TABLE `filemanager_owners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m140524_153638_init_user',1423418344),('m140524_153642_init_user_auth',1423418344),('m140805_084737_file_storage_item',1423644396),('m141129_130551_create_filemanager_mediafile_table',1423734420),('m141203_173402_create_filemanager_owners_table',1423734420),('m141203_175538_add_filemanager_owners_ref_mediafile_fk',1423734420);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` mediumtext,
  `url_name` varchar(255) DEFAULT NULL COMMENT 'sluggified from title by default also will published under URL like http://raconteur.com/ClientName/PageName ',
  `created` int(10) NOT NULL,
  `modified` int(10) NOT NULL,
  `status` enum('removed','published','pending','unpublished') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`id`),
  KEY `fk_page_user_id_idx` (`user_id`),
  KEY `fk_template_id_idx` (`template_id`),
  CONSTRAINT `fk_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_template_id` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profile_user_id` (`user_id`),
  CONSTRAINT `profile_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,2,'alex','lazarev','arsey co.','boss',NULL,'2015-02-10 12:37:59',NULL),(2,1,'111','111','111','','48',NULL,'2015-02-12 14:48:14');
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `can_admin` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'Admin','2015-02-08 18:59:04',NULL,1),(2,'User','2015-02-08 18:59:04',NULL,0);
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script`
--

DROP TABLE IF EXISTS `script`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script`
--

LOCK TABLES `script` WRITE;
/*!40000 ALTER TABLE `script` DISABLE KEYS */;
INSERT INTO `script` VALUES (1,1,'123','sfd','js');
/*!40000 ALTER TABLE `script` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_section_assignment`
--

DROP TABLE IF EXISTS `script_section_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_section_assignment` (
  `script_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`script_id`,`section_id`),
  KEY `fk_ssa_script_id_idx` (`script_id`),
  KEY `fk_ssa_section_id_idx` (`section_id`),
  CONSTRAINT `fk_ssa_script_id` FOREIGN KEY (`script_id`) REFERENCES `script` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ssa_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_section_assignment`
--

LOCK TABLES `script_section_assignment` WRITE;
/*!40000 ALTER TABLE `script_section_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `script_section_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `script_template_assignment`
--

DROP TABLE IF EXISTS `script_template_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `script_template_assignment` (
  `script_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  `order` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`script_id`,`template_id`),
  KEY `fk_sta_template_id_idx` (`template_id`),
  KEY `fk_sta_script_id_idx` (`script_id`),
  CONSTRAINT `fk_staa_script_id` FOREIGN KEY (`script_id`) REFERENCES `script` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_staa_template_id` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `script_template_assignment`
--

LOCK TABLES `script_template_assignment` WRITE;
/*!40000 ALTER TABLE `script_template_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `script_template_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `preview_img` varchar(255) DEFAULT NULL,
  `html` longtext,
  `shared` enum('no','yes') NOT NULL DEFAULT 'no',
  `type` varchar(50) NOT NULL,
  `created` int(10) NOT NULL,
  `modified` int(10) NOT NULL,
  `status` enum('removed','published','pending','unpublished') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`id`),
  KEY `fk_section_user_id_idx` (`user_id`),
  CONSTRAINT `fk_section_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` char(40) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('090sp8lr5rioskng9jsq7g9pp1',1426341485,'__flash|a:0:{}__id|i:1;'),('1cgss2tonbce1rbbn5uidv8q47',1426078235,'__flash|a:0:{}'),('2uoc178lqbnu564c1a8pstonv2',1426077996,'__flash|a:0:{}__returnUrl|s:12:\"/v1/requests\";'),('3939qvo7b12roj12s5tf6hgep2',1426246802,'__flash|a:0:{}'),('5r81gt0s7i4tfgslps16ah1f15',1426077472,'__flash|a:0:{}'),('61k5hhbq8ds5eutjrrm89ajgb4',1426078219,'__flash|a:0:{}'),('7iuqfskjjdn5bl3gtt6cia7m06',1426177983,'__flash|a:0:{}'),('979e4mfq2gcctrcfg42u4km705',1426077369,'__flash|a:0:{}__id|i:1;'),('a3k1m6k3vprofod0pa67uhrjc6',1426082507,'__flash|a:0:{}'),('duh525ntcghb5d7mnnh0loqfk7',1426078009,'__flash|a:0:{}__id|i:1;'),('ejq7llabgbsjlie1isoigfpst0',1426192338,'__flash|a:0:{}'),('gbr1d6qdvf9qdr3m0a2fr0jbe2',1426078200,'__flash|a:0:{}'),('h5qqhuoa7kdipmk9nvfo0s0261',1426094048,'__flash|a:0:{}'),('hahdi6pvl9f8s9e95f4us044u5',1426178002,'__flash|a:0:{}'),('inmkj76lv4afft67svlp6b87b5',1426144868,'__flash|a:0:{}'),('ld5551qu3f47js9bsrjt7dha34',1426188286,'__flash|a:0:{}'),('mgpqa3bc01c3gb06ifoiqp1fg5',1426078009,'__flash|a:0:{}'),('npu53lgikh55ou7pos5nga6aa2',1426077369,'__flash|a:0:{}'),('o8gdgj5hprugtbbd8c5a194952',1426182423,'__flash|a:0:{}'),('pd7iolpiupg1gk4pp9fhfclgq5',1426077397,'__flash|a:0:{}__returnUrl|s:12:\"/v1/requests\";'),('piqa930qd6ku93soaa3ke795a1',1426188313,'__flash|a:0:{}'),('qs8impomrfdgkggpdj8gcsc1r5',1426078235,'__flash|a:0:{}__id|i:1;'),('rrnrlo1vkppsdn1mquc1ipu453',1426163710,'__flash|a:0:{}'),('uik93p9ougf00klr3kno8sp436',1426078219,'__flash|a:0:{}__id|i:1;'),('ustbjrf26e3c2pv5qatvjtihf7',1426077710,'__flash|a:0:{}__returnUrl|s:12:\"/v1/requests\";');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `created` int(10) NOT NULL,
  `modified` int(10) NOT NULL,
  `status` enum('removed','published','pending','unpublished') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_template_assignment`
--

DROP TABLE IF EXISTS `subscription_template_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_template_assignment` (
  `subscription_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`,`template_id`),
  UNIQUE KEY `UNIQUE_sta_sbuscription_id_and_template_id` (`subscription_id`,`template_id`),
  KEY `fk_sta_template_id_idx` (`template_id`),
  KEY `fk_sta_subscription_id_idx` (`subscription_id`),
  CONSTRAINT `fk_sta_subscription_id` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_sta_template_id` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_template_assignment`
--

LOCK TABLES `subscription_template_assignment` WRITE;
/*!40000 ALTER TABLE `subscription_template_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription_template_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_user_assignment`
--

DROP TABLE IF EXISTS `subscription_user_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_user_assignment` (
  `subscription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`subscription_id`,`user_id`),
  UNIQUE KEY `UNIQUE_sua_user_id_and_subscription_id` (`subscription_id`,`user_id`),
  KEY `fk_sua_user_id_idx` (`user_id`),
  KEY `fk_sua_subscription_id_idx` (`subscription_id`),
  CONSTRAINT `fk_sua_subscription_id` FOREIGN KEY (`subscription_id`) REFERENCES `subscription` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_sua_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_user_assignment`
--

LOCK TABLES `subscription_user_assignment` WRITE;
/*!40000 ALTER TABLE `subscription_user_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription_user_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template`
--

DROP TABLE IF EXISTS `template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `preview_img` varchar(255) DEFAULT NULL,
  `head_html` mediumtext,
  `is_free` enum('no','yes') NOT NULL DEFAULT 'no',
  `created` int(10) NOT NULL,
  `modified` int(10) NOT NULL,
  `status` enum('removed','published','pending','unpublished') NOT NULL DEFAULT 'published',
  PRIMARY KEY (`id`),
  KEY `fk_user_id_idx` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template`
--

LOCK TABLES `template` WRITE;
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
/*!40000 ALTER TABLE `template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_section_assignment`
--

DROP TABLE IF EXISTS `template_section_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_section_assignment` (
  `template_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`section_id`),
  KEY `fk_tsa_section_id_idx` (`section_id`),
  KEY `fk_tsa_template_id_idx` (`template_id`),
  CONSTRAINT `fk_tsa_section_id` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_tsa_template_id` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_section_assignment`
--

LOCK TABLES `template_section_assignment` WRITE;
/*!40000 ALTER TABLE `template_section_assignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `template_section_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_time` timestamp NULL DEFAULT NULL,
  `create_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `ban_time` timestamp NULL DEFAULT NULL,
  `ban_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`email`),
  UNIQUE KEY `user_username` (`username`),
  KEY `user_role_id` (`role_id`),
  CONSTRAINT `user_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,1,'neo@neo.com',NULL,'neo','$2y$10$WYB666j7MmxuW6b.kFTOde/eGCLijWa6BFSjAAiiRbSAqpC1HCmrC','Gw2O1zzv2VMKlhrmg4Tz18qy3lQRZ_7q','pkHhwF0UroeA5jL2xQFy-NNuffIvuWRL','10.2.2.1','2015-02-12 10:13:48',NULL,'2015-02-08 18:59:04',NULL,NULL,NULL),(2,2,1,'arseysensector@gmail.com',NULL,'aresy','$2y$13$i0BDl5v/TvOl/n/M2tEyZeYc6R2324sYdIU6oN47E0pzz1nUD3x5O',NULL,NULL,'10.2.2.1','2015-02-10 22:06:13',NULL,'2015-02-10 12:37:59','2015-02-10 22:05:32',NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_auth`
--

DROP TABLE IF EXISTS `user_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_attributes` text COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_auth_provider_id` (`provider_id`),
  KEY `user_auth_user_id` (`user_id`),
  CONSTRAINT `user_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_auth`
--

LOCK TABLES `user_auth` WRITE;
/*!40000 ALTER TABLE `user_auth` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_auth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_key`
--

DROP TABLE IF EXISTS `user_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `consume_time` timestamp NULL DEFAULT NULL,
  `expire_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_key_key` (`key`),
  KEY `user_key_user_id` (`user_id`),
  CONSTRAINT `user_key_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_key`
--

LOCK TABLES `user_key` WRITE;
/*!40000 ALTER TABLE `user_key` DISABLE KEYS */;
INSERT INTO `user_key` VALUES (1,2,3,'yCOc2EaHYjdhUV08cyo4lWui_ExGFo0J','2015-02-10 21:43:57','2015-02-10 22:03:47','2015-02-12 21:43:57'),(2,2,3,'fT20Ez7dV8S-2-OEDRO4gYHrs7eflw-t','2015-02-10 22:04:59','2015-02-10 22:05:32','2015-02-12 22:04:59');
/*!40000 ALTER TABLE `user_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_request`
--

DROP TABLE IF EXISTS `user_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `type` enum('new_template') NOT NULL DEFAULT 'new_template',
  `created` int(10) DEFAULT NULL,
  `modified` int(10) DEFAULT NULL,
  `status` enum('pending','approved','declined') NOT NULL DEFAULT 'pending',
  PRIMARY KEY (`id`),
  KEY `fk_request_user_id_idx` (`user_id`),
  CONSTRAINT `fk_request_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_request`
--

LOCK TABLES `user_request` WRITE;
/*!40000 ALTER TABLE `user_request` DISABLE KEYS */;
INSERT INTO `user_request` VALUES (1,1,'sadgqe4g','new_template',1423555617,1423573245,'pending'),(2,1,'123','new_template',1423555628,1423555628,'pending'),(3,1,'123','new_template',1423555633,1423555633,'pending'),(4,1,'123','new_template',1423555633,1423555633,'pending'),(5,1,'123','new_template',1423555633,1423555633,'pending'),(6,1,'123','new_template',1423555633,1423555633,'pending'),(7,1,'123','new_template',1423555675,1423555675,'pending'),(8,1,'123','new_template',1423555677,1423555677,'pending'),(9,1,'123','new_template',1423555711,1423555711,'pending'),(10,1,'123','new_template',1423555867,1423555867,'approved'),(11,1,'123','new_template',1423555973,1423555973,'pending'),(12,1,'123','new_template',1423556111,1423556111,'pending'),(13,1,'123','new_template',1423556121,1423556121,'pending'),(14,1,'123','new_template',1423556123,1423556123,'pending'),(15,1,'123','new_template',1423556123,1423556123,'pending'),(16,1,'123','new_template',1423556124,1423556124,'pending'),(17,1,'123','new_template',1423556124,1423556124,'pending'),(18,1,'123','new_template',1423556125,1423556125,'pending'),(19,1,'123','new_template',1423556125,1423556125,'pending'),(20,1,'123','new_template',1423556126,1423556126,'pending'),(21,1,'123','new_template',1423556126,1423556126,'pending'),(22,1,'123','new_template',1423556126,1423556126,'pending'),(23,1,'123','new_template',1423556127,1423556127,'pending'),(24,1,'123','new_template',1423556127,1423556127,'pending'),(25,1,'123','new_template',1423556127,1423556127,'pending'),(26,1,'123','new_template',1423556127,1423556127,'pending'),(27,1,'123','new_template',1423556127,1423556127,'pending'),(28,1,'123','new_template',1423556127,1423556127,'pending'),(29,1,'123','new_template',1423556128,1423556128,'pending'),(30,1,'123','new_template',1423556128,1423556128,'pending'),(31,1,'123','new_template',1423556128,1423556128,'pending'),(32,1,'123','new_template',1423556128,1423556128,'pending'),(33,1,'123','new_template',1423556128,1423556128,'pending'),(34,1,'123','new_template',1423556128,1423556128,'pending'),(35,1,'123','new_template',1423556129,1423556129,'pending'),(36,1,'123','new_template',1423556129,1423556129,'pending'),(37,1,'123','new_template',1423556129,1423556129,'pending'),(38,1,'123','new_template',1423556129,1423556129,'pending'),(39,1,'123','new_template',1423556129,1423556129,'pending'),(40,1,'123','new_template',1423556129,1423556129,'pending'),(41,1,'123','new_template',1423556130,1423556130,'pending'),(42,1,'123','new_template',1423556130,1423556130,'pending'),(43,1,'123','new_template',1423556130,1423556130,'pending'),(44,1,'123','new_template',1423556130,1423556130,'pending'),(45,1,'123','new_template',1423556130,1423594631,'pending'),(46,1,'123','new_template',1423556130,1423556130,'pending'),(47,1,'123','new_template',1423556132,1423556132,'pending'),(48,1,'123','new_template',1423556132,1423556132,'pending'),(49,1,'123','new_template',1423556132,1423556132,'pending'),(50,1,'123','new_template',1423556132,1423556132,'pending'),(51,1,'123','new_template',1423556132,1423556132,'pending'),(52,1,'123','new_template',1423556133,1423556133,'pending'),(53,1,'123','new_template',1423556133,1423556133,'pending'),(54,1,'123','new_template',1423556133,1423556133,'pending'),(55,1,'123','new_template',1423556133,1423556133,'pending'),(56,1,'123','new_template',1423556133,1423556133,'pending'),(57,1,'123','new_template',1423556133,1423556133,'pending'),(58,1,'123','new_template',1423556134,1423556134,'pending'),(59,1,'123','new_template',1423556134,1423556134,'pending'),(60,1,'123','new_template',1423556134,1423556134,'pending'),(61,1,'123','new_template',1423556239,1423556239,'pending'),(62,1,'123','new_template',1423556244,1423556244,'pending'),(63,2,NULL,'new_template',1423590705,1423590705,'pending'),(64,2,'','new_template',1423590710,1423590710,'pending'),(65,2,'','new_template',1423590711,1423590711,'pending'),(66,2,'','new_template',1423590714,1423590714,'pending'),(67,2,'','new_template',1423593456,1423593456,'pending'),(68,2,'','new_template',1423593517,1423593517,'pending'),(69,1,'111111','new_template',1423654647,1423654717,'pending');
/*!40000 ALTER TABLE `user_request` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-12 22:22:08
